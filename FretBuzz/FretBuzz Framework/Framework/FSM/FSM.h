#pragma once
#include <vector>
#include <string>
#include "../Debugging/Debug.h"

namespace FretBuzzManager
{
	class IFSM
	{
	protected:
		std::string m_strSceneName;

	public:
		virtual void OnStateEnter() = 0
		{
			FretBuzzSystem::Debug::Log("State Entered :: '" + m_strSceneName+ "'");
		}

		virtual void OnStateExit() = 0
		{
			FretBuzzSystem::Debug::Log("State Exited :: '" + m_strSceneName + "'");
		}

		inline const std::string GetSceneName() const
		{
			return m_strSceneName;
		}

		inline IFSM(std::string a_strSceneName = "")
		{
			m_strSceneName = a_strSceneName;
		}
	};

	template<typename T_STATE>
	class FSM
	{
	protected:
		bool m_bIsTransitionToSelfAllowed = false;

		FSM(T_STATE* a_pStartState, bool a_bIsTransitionToSelfAllowed = false)
			: m_pCurrentState{a_pStartState},
			m_bIsTransitionToSelfAllowed{ a_bIsTransitionToSelfAllowed }
		{
			m_pCurrentState->OnStateEnter();
			FretBuzzSystem::Debug::Log("InitialState :: '" + m_pCurrentState->GetSceneName() + "'");
			RegisterState(a_pStartState);
		};

		FSM(std::vector<T_STATE*> a_pVectState, bool a_bIsTransitionToSelfAllowed = false)
			: m_pCurrentState{ a_pVectState[0] },
			m_bIsTransitionToSelfAllowed{ a_bIsTransitionToSelfAllowed }
		{
			m_pCurrentState->OnStateEnter();
			FretBuzzSystem::Debug::Log("InitialState :: '" + m_pCurrentState->GetSceneName() +"'");

			for (auto& l_State : a_pVectState)
			{
				RegisterState(l_State);
			}

			FretBuzzSystem::Debug::Log("Total num of states: : '"+std::to_string(m_vectStates.size())+"'");
		};

		virtual ~FSM() {};

		std::vector<IFSM*> m_vectStates;

		IFSM* m_pCurrentState = nullptr;

		void TransitionTo(std::string a_strTransitionTo)
		{
			IFSM* l_pRegisteredState = GetStateRegisteredWithID(a_strTransitionTo);
			if (l_pRegisteredState == nullptr)
			{
				FretBuzzSystem::Debug::LogError("FSM::RegisterState:: State with name '" + a_strTransitionTo + "' does not exist in the state list.");
				return;
			}

			if ((l_pRegisteredState == m_pCurrentState) && !m_bIsTransitionToSelfAllowed)
			{
				return;
			}

			m_pCurrentState->OnStateExit();
			m_pCurrentState = l_pRegisteredState;
			m_pCurrentState->OnStateEnter();
		}

		///Registers the state with name to the vect list of states if it does not already exist
		void RegisterState(T_STATE* a_pStateToRegister)
		{
			IFSM* l_pCurrentState = dynamic_cast<IFSM*>(a_pStateToRegister);

			if (l_pCurrentState == nullptr)
			{
				FretBuzzSystem::Debug::LogError("FSM::RegisterState:: State is not a derived class of IFSM.");
				return;
			}

			if (GetStateRegisteredWithID(l_pCurrentState->GetSceneName()) != nullptr)
			{
				FretBuzzSystem::Debug::LogError("FSM::RegisterState:: State with name '" + l_pCurrentState->GetSceneName() + "' already exists in the list of states.");
				return;
			}

			m_vectStates.emplace_back(a_pStateToRegister);
		}

		///Checks if the state with given state name already exists in the vector of all states.
		IFSM* GetStateRegisteredWithID(const std::string& a_strStateName)
		{
			int l_iStateCount = m_vectStates.size();

			for (int l_iStateIndex = 0; l_iStateIndex < l_iStateCount; l_iStateIndex++)
			{
				if (std::strcmp(a_strStateName.c_str(), m_vectStates[l_iStateIndex]->GetSceneName().c_str()) == 0)
				{
					return m_vectStates[l_iStateIndex];
				}
			}
			return nullptr;
		}
	};
}