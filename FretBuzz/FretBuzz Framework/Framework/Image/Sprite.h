#pragma once
#include "../Graphics/PixelColor.h"
#include "../Graphics/Graphics.h"
#include <functional>

namespace FretBuzzEngine
{
	enum class InvertType
	{
		INVERT_VERTICAL,
		INVERT_HORIZONTAL,
		INVERT_VERT_AND_HORIZ,
		DONT_INVERT
	};

	class Sprite
	{
	protected:
		friend class Spritesheet;

		static constexpr int l_iExtensionCharLength = 4;

		FretBuzzSystem::PixelColor* m_pSpriteColor = nullptr;
		FretBuzzSystem::PixelColor m_ColorKey = NULL;
		bool m_bIsContainsColorKey = false;

		void InvertHorizontal();
		void InvertVertical();

		unsigned int m_iWidth = 0;
		unsigned int m_iHeight = 0;

	public:
		
		Sprite(const std::string& a_strFileName, bool a_bIsContainsColorKey = false, FretBuzzSystem::PixelColor a_ColorKey = NULL);
		Sprite(const std::string& a_strFileName, InvertType a_enInvertType, bool a_bIsContainsColorKey = false, FretBuzzSystem::PixelColor a_ColorKey = NULL);
		Sprite(const Sprite* const a_pSpriteSheet, int a_iXSpriteSheetPosition, int a_iYSpriteSheetPosition, int a_iSpriteWidth, int a_iSpriteHeight);
		virtual ~Sprite();

		virtual void DrawSprite(int a_iX, int a_iY, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);
		virtual void DrawSprite(FretBuzzMath::Mat3& a_Mat3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);

		int GetWidth() const;
		int GetHeight() const;

	};
}