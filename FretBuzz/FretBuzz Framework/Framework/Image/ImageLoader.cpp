#pragma once
#include "ImageLoader.h"
#include <windows.h>
#include <fstream>
#include <cassert>
#include "../Debugging/Debug.h"

namespace FretBuzzEngine
{
	const std::string ImageLoader::STR_EXTENSION_PNG = ".png";
	const std::string ImageLoader::STR_EXTENSION_BMP = ".bmp";
	const std::string ImageLoader::STR_RESOURCES_DIRECTORY = "Resources\\Sprite\\";

	//Load bitmap using the inbuilt FretBuzz engine method of loading a bitmap
	void ImageLoader::LoadBMP(std::ifstream& a_ImageFileStream, FretBuzzSystem::PixelColor*& a_pPixelColor, unsigned int& a_iWidth, unsigned int& a_iHeight)
	{
		assert(a_ImageFileStream && "ImageLoader::LoadBMP_FretBuzz:: Could not read BMP file");

		BITMAPFILEHEADER l_BitmapFileHeader;
		a_ImageFileStream.read((char*)(&l_BitmapFileHeader), sizeof(l_BitmapFileHeader));

		BITMAPINFOHEADER l_BitmapInfoHeader;
		a_ImageFileStream.read((char*)(&l_BitmapInfoHeader), sizeof(l_BitmapInfoHeader));

		assert(l_BitmapInfoHeader.biBitCount == 24 && "ImageLoader::LoadBMP_FretBuzz:: Bitcount is not 24.");
		assert(l_BitmapInfoHeader.biCompression == BI_RGB && "ImageLoader::LoadBMP_FretBuzz:: Image is not uncompressed.");
		
		//Checks if the bfType is 'BM'
		assert((l_BitmapFileHeader.bfType == 0x4D42) && "ImageLoader::LoadBMP_FretBuzz:: Incorrect bitmap type encountered while passing.");

		a_iHeight = l_BitmapInfoHeader.biHeight;
		a_iWidth = l_BitmapInfoHeader.biWidth;

		int l_iTotlaPixels = a_iHeight * a_iWidth;
		a_pPixelColor = new FretBuzzSystem::PixelColor[l_iTotlaPixels];

		a_ImageFileStream.seekg(l_BitmapFileHeader.bfOffBits, std::ios::beg);
		int l_iRowPadding = (4 - ((a_iWidth * 3) % 4)) % 4;

		for (int l_iYIndex = 1; l_iYIndex <= a_iHeight; l_iYIndex++)
		{
			for (int l_iXIndex = 0; l_iXIndex < a_iWidth; l_iXIndex++)
			{
				a_pPixelColor[((a_iHeight - l_iYIndex) * a_iWidth) + (l_iXIndex)] = FretBuzzSystem::PixelColor(a_ImageFileStream.get(), a_ImageFileStream.get(), a_ImageFileStream.get(), 255);
			}
			a_ImageFileStream.seekg(l_iRowPadding, std::ios::cur);
		}
	}

	//Load PNG using the Lode PNG library
	void ImageLoader::LoadPNG(std::string& const a_strFilePath, FretBuzzSystem::PixelColor*& a_pPixelColor, const int& a_iBitDepth, const LodePNGColorType a_PNGColorType,
		unsigned int& a_iWidth, unsigned int& a_iHeight)
	{
		if (a_iBitDepth != 8)
		{
			std::string l_strError = "ImageLoader::LoadPNG_Lode:: Image depth of '" + std::to_string(a_iBitDepth) +"' bits is not supported";
			FretBuzzSystem::Debug::LogError(l_strError);
			throw std::exception(l_strError.c_str());
			return;
		}

		std::vector<unsigned char> l_vectPixelByte;
		lodepng::decode(l_vectPixelByte, a_iWidth, a_iHeight, a_strFilePath, a_PNGColorType, a_iBitDepth);

		unsigned int l_iTotalNoOfPixel = a_iWidth * a_iHeight;
		a_pPixelColor = new FretBuzzSystem::PixelColor[l_iTotalNoOfPixel];

		switch (a_PNGColorType)
		{
			case LCT_GREY:
			{
				//TODO::
				break;
			}

			case LCT_RGB:
			{
				for (int l_iPixelIndex = 0, l_iCharIndex = 0; l_iPixelIndex < l_iTotalNoOfPixel; l_iPixelIndex++, l_iCharIndex++)
				{
					FretBuzzSystem::PixelColor l_CurrPixelColor(0xFF000000);
					l_CurrPixelColor.SetRed(l_vectPixelByte[l_iCharIndex]);
					l_CurrPixelColor.SetGreen(l_vectPixelByte[++l_iCharIndex]);
					l_CurrPixelColor.SetBlue(l_vectPixelByte[++l_iCharIndex]);
					a_pPixelColor[l_iPixelIndex] = l_CurrPixelColor;
				}

				break;
			}

			case LCT_PALETTE:
			{
				//TODO::
				break;
			}

			case LCT_GREY_ALPHA:
			{
				//TODO::
				break;
			}

			case LCT_RGBA:
			{
				for (int l_iPixelIndex = 0, l_iCharIndex = 0; l_iPixelIndex < l_iTotalNoOfPixel; l_iPixelIndex++, l_iCharIndex++)
				{
					FretBuzzSystem::PixelColor l_CurrPixelColor(0);
					l_CurrPixelColor.SetRed(l_vectPixelByte[l_iCharIndex]);
					l_CurrPixelColor.SetGreen(l_vectPixelByte[++l_iCharIndex]);
					l_CurrPixelColor.SetBlue(l_vectPixelByte[++l_iCharIndex]);
					l_CurrPixelColor.SetAlpha(l_vectPixelByte[++l_iCharIndex]);

					a_pPixelColor[l_iPixelIndex] = l_CurrPixelColor.CalculateAlphaIntoRGB(); //Calculate the RGB with respect to its alpha
				}
				break;
			}

			default:
			{
				std::string l_strError = "ImageLoader::LoadPNG:: Image color of type '" + std::to_string(a_PNGColorType) + "' is not supported";
				FretBuzzSystem::Debug::LogError(l_strError);
				throw std::exception(l_strError.c_str());
				break;
			}
		}
	}

	//Loads the sprite with given file path into the pixel color
	//Checks chich format the file is in example: .png .bmp
	//Loads the image with the format, calls the function for loading .bmp or .png
	void ImageLoader::LoadSprite(const std::string& a_strFileName, FretBuzzSystem::PixelColor*& a_pPixelColor, unsigned int& a_iWidth, unsigned int& a_iHeight)
	{
		std::string l_strImagePath = STR_RESOURCES_DIRECTORY + a_strFileName;

		std::ifstream l_FileInputStreamImage(l_strImagePath, std::ios::binary);
		if (!l_FileInputStreamImage)
		{
			std::string l_strError = "Sprite::Sprite::Could not find image file with name :: '" + l_strImagePath + "'";
			FretBuzzSystem::Debug::LogError("Sprite::Sprite::Could not find image file with name :: '" + l_strImagePath + "'");
			throw std::exception(l_strError.c_str());
			return;
		}

		unsigned int l_iFileExtensionStart = a_strFileName.length() - EXTENSION_CHAR_LENGTH;

		std::string l_strFileExtension = a_strFileName.substr(l_iFileExtensionStart);

		if (l_strFileExtension.compare(STR_EXTENSION_PNG) == 0)
		{

		/* Byte 24 in the PNG file determines the bit depth i.e (1, 2, 4, 8, or 16).
		   Byte 25 in the PNG file determines the color type of the PNG
						0 = Greyscale,
						2 = RGB,
						3 = Palette,
						4 = greyscale with alpha
						6 = RGBA.
		*/
			l_FileInputStreamImage.seekg(24, std::ios::beg);
			int l_iBitDepth = l_FileInputStreamImage.get();
			int l_iColorType = l_FileInputStreamImage.get();
			LoadPNG(l_strImagePath, a_pPixelColor, l_iBitDepth, (LodePNGColorType)l_iColorType, a_iWidth, a_iHeight);
		}
		else if(l_strFileExtension.compare(STR_EXTENSION_BMP) == 0)
		{
			LoadBMP(l_FileInputStreamImage, a_pPixelColor, a_iWidth, a_iHeight);
		}
		else
		{
			l_FileInputStreamImage.close();
			std::string l_strError = "ImageLoader::LoadSprite:: Image format of file :: '" + a_strFileName + "' is not supported.";
			FretBuzzSystem::Debug::LogError(l_strError);
			throw std::exception(l_strError.c_str());
		}

		l_FileInputStreamImage.close();
	}
}