#pragma once
#include "Poly.h"
#include "../Libs/dxflib/dl_dxf.h"
#include "../Debugging/Debug.h"

namespace FretBuzzEngine
{
#pragma region Poly
	
	const std::string Poly:: DXF_FOLDER_PATH = "Resources\\DXF\\";

	Poly::Poly(const std::string& a_strDXFFileName)
	{
		const std::string FILE_PATH = DXF_FOLDER_PATH + a_strDXFFileName;

		DL_Dxf* l_pDXF = new DL_Dxf();

		if (!l_pDXF->in(FILE_PATH, this))
		{
			std::string l_strError = "PolyLoader::PolyLoader:: Failed to load poly from file with filename '" + a_strDXFFileName;
			FretBuzzSystem::Debug::LogError(l_strError);
			throw std::exception(l_strError.c_str());
		}

		if (l_pDXF != nullptr)
		{
			delete l_pDXF;
			l_pDXF = nullptr;
		}
	}

	//Adds the vertex data into the vector of vertices
	void Poly::addVertex(const DL_VertexData& data)
	{
		m_VertexVect.push_back(FretBuzzMath::Vec2(data.x, -data.y));
	}

	Poly::~Poly()
	{
	}

	//Draws the poly by connecting one vertex to the next with a line
	void Poly::DrawPoly(FretBuzzMath::Vec2 a_Position, FretBuzzSystem::Graphics& a_GFX, FretBuzzSystem::PixelColor a_PixelColor)
	{
		int l_izise = m_VertexVect.size();

		auto l_vertStart = m_VertexVect.begin(),
			l_vertEnd = m_VertexVect.end() - 1;

		for (;
			l_vertStart != l_vertEnd;
			l_vertStart++
			)
		{
			a_GFX.DrawLine((*l_vertStart) + a_Position, *(l_vertStart + 1) + a_Position, a_PixelColor);
		}
		a_GFX.DrawLine((*l_vertEnd) + a_Position, *(m_VertexVect.begin() + 1) + a_Position, a_PixelColor);
	}

#pragma endregion Poly
}