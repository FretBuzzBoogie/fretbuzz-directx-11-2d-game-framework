#pragma once

#include "Tex2D.h"
#include "../Math/Mat3.h"

namespace FretBuzzEngine
{
	Tex2D::Tex2D(const std::string& a_strFileName, bool a_bIsContainsColorKey, FretBuzzSystem::PixelColor a_ColorKey)
		: Sprite(a_strFileName, a_bIsContainsColorKey, a_ColorKey)
	{
		Init();
	}

	Tex2D::Tex2D(const Sprite* const a_pSpriteSheet, int a_iXSpriteSheetPosition, int a_iYSpriteSheetPosition, int a_iSpriteWidth, int a_iSpriteHeight)
		: Sprite(a_pSpriteSheet, a_iXSpriteSheetPosition, a_iYSpriteSheetPosition, a_iSpriteWidth, a_iSpriteHeight)
	{
		Init();
	}

	void Tex2D::Init()
	{
		const float l_fHalfWidth = (float)m_iWidth / 2.0f;
		const float l_fHalfHeight = (float)m_iHeight / 2.0f;

		m_arrV2StartVecPos =
		{
			FretBuzzMath::Vec2{ -l_fHalfWidth, -l_fHalfHeight },
			FretBuzzMath::Vec2{ l_fHalfWidth, -l_fHalfHeight },
			FretBuzzMath::Vec2{ l_fHalfWidth, l_fHalfHeight },
			FretBuzzMath::Vec2{ -l_fHalfWidth, l_fHalfHeight }
		};

		m_arrVertex[0] = { { 0.0f, 0.0f }	, m_arrV2StartVecPos[0] };
		m_arrVertex[1] = { { (float)m_iWidth - 1.0f, 0.0f }, m_arrV2StartVecPos[1] };
		m_arrVertex[2] = { { (float)m_iWidth - 1.0f, (float)m_iHeight - 1.0f }, m_arrV2StartVecPos[2] };
		m_arrVertex[3] = { { 0.0f, (float)m_iHeight - 1.0f } , m_arrV2StartVecPos[3] };
	}

	Tex2D::~Tex2D()
	{

	}

	void Tex2D::DrawSprite(FretBuzzMath::Mat3& a_Transform, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		m_arrVertex[0].m_v2Model = a_Transform * m_arrV2StartVecPos[0];
		m_arrVertex[1].m_v2Model = a_Transform * m_arrV2StartVecPos[1];
		m_arrVertex[2].m_v2Model = a_Transform * m_arrV2StartVecPos[2];
		m_arrVertex[3].m_v2Model = a_Transform * m_arrV2StartVecPos[3];

		DrawTexTriangle(m_arrVertex[0], m_arrVertex[1], m_arrVertex[3], a_GFX, a_Rect);
		DrawTexTriangle(m_arrVertex[1], m_arrVertex[2], m_arrVertex[3], a_GFX, a_Rect);
	}

	void Tex2D::DrawTexTriangle(FretBuzzMath::Vertex2D a_vert0, FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		if (a_vert0.m_v2Model.Y > a_vert1.m_v2Model.Y)
		{
			a_vert0.Swap(a_vert1);
		}

		if (a_vert0.m_v2Model.Y > a_vert2.m_v2Model.Y)
		{
			a_vert0.Swap(a_vert2);
		}

		if (a_vert1.m_v2Model.Y > a_vert2.m_v2Model.Y)
		{
			a_vert1.Swap(a_vert2);
		}

		if (a_vert0.m_v2Model.Y == a_vert1.m_v2Model.Y)
		{
			if (a_vert0.m_v2Model.X > a_vert1.m_v2Model.X)
			{
				a_vert0.Swap(a_vert1);
			}

			DrawFlatTopTriangle(a_vert0, a_vert1, a_vert2, a_GFX, a_Rect);
		}
		else if (a_vert1.m_v2Model.Y == a_vert2.m_v2Model.Y)
		{
			if (a_vert1.m_v2Model.X > a_vert2.m_v2Model.X)
			{
				a_vert1.Swap(a_vert2);
			}

			DrawFlatBottomTriangle(a_vert0, a_vert1, a_vert2, a_GFX, a_Rect);
		}
		else
		{
			FretBuzzMath::Vec2 l_v2Model = { ((a_vert2.m_v2Model.X - a_vert0.m_v2Model.X) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y)) *
				(a_vert1.m_v2Model.Y - a_vert0.m_v2Model.Y) + a_vert0.m_v2Model.X ,  a_vert1.m_v2Model.Y };

			FretBuzzMath::Vec2 l_v2Texture = { a_vert0.m_v2Texture + (a_vert2.m_v2Texture - a_vert0.m_v2Texture) * ((l_v2Model.Y - a_vert0.m_v2Model.Y) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y)) };

			const FretBuzzMath::Vertex2D l_vertNew{ l_v2Texture , l_v2Model };

			if (l_vertNew.m_v2Model.X > a_vert1.m_v2Model.X) //Major right
			{
				DrawFlatTopTriangle(a_vert1, l_vertNew, a_vert2,a_GFX, a_Rect);
				DrawFlatBottomTriangle(a_vert0, a_vert1, l_vertNew, a_GFX, a_Rect);
			}
			else //Major left
			{
				DrawFlatTopTriangle(l_vertNew, a_vert1, a_vert2, a_GFX, a_Rect);
				DrawFlatBottomTriangle(a_vert0, l_vertNew, a_vert1, a_GFX, a_Rect);
			}
		}
	}

	void Tex2D::DrawFlatTopTriangle(FretBuzzMath::Vertex2D a_vert0, FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		int l_iYStart = max((int)ceil(a_vert0.m_v2Model.Y), a_Rect->m_fTop);
		int l_iYEnd = min((int)ceil(a_vert2.m_v2Model.Y) - 1, a_Rect->m_fBottom);

		float l_fLeftSlope = (a_vert2.m_v2Model.X - a_vert0.m_v2Model.X) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y);
		float l_fRightSlope = (a_vert2.m_v2Model.X - a_vert1.m_v2Model.X) / (a_vert2.m_v2Model.Y - a_vert1.m_v2Model.Y);

		FretBuzzMath::Vec2 l_v2LeftTextureSingleStep = (a_vert2.m_v2Texture - a_vert0.m_v2Texture) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y);
		FretBuzzMath::Vec2 l_v2RightTextureSingleStep = (a_vert2.m_v2Texture - a_vert1.m_v2Texture) / (a_vert2.m_v2Model.Y - a_vert1.m_v2Model.Y);

		FretBuzzMath::Vec2 l_v2LeftPrestep = a_vert0.m_v2Texture  + l_v2LeftTextureSingleStep * (l_iYStart - a_vert0.m_v2Model.Y);
		FretBuzzMath::Vec2 l_v2RightPrestep = a_vert1.m_v2Texture + l_v2RightTextureSingleStep * (l_iYStart - a_vert0.m_v2Model.Y);

		for (int l_iYIndex = l_iYStart; l_iYIndex <= l_iYEnd; l_iYIndex++,
			l_v2LeftPrestep += l_v2LeftTextureSingleStep, l_v2RightPrestep += l_v2RightTextureSingleStep)
		{
			float l_fXLeft = (l_fLeftSlope *  ((float)l_iYIndex - a_vert0.m_v2Model.Y)) + a_vert0.m_v2Model.X;
			float l_fXRight = (l_fRightSlope *  ((float)l_iYIndex - a_vert0.m_v2Model.Y)) + a_vert1.m_v2Model.X;

			int l_iXStart = max((int)ceil(l_fXLeft), a_Rect->m_fLeft);
			int l_iXEnd = min((int)ceil(l_fXRight) - 1, a_Rect->m_fRight);

			FretBuzzMath::Vec2 l_v2ScanStep = (l_v2RightPrestep - l_v2LeftPrestep)/ (l_fXRight - l_fXLeft);
			FretBuzzMath::Vec2 l_v2Start = l_v2LeftPrestep + l_v2ScanStep * (l_iXStart - l_fXLeft);

			
			for (int l_iXIndex = l_iXStart; l_iXIndex <= l_iXEnd; l_iXIndex++, l_v2Start += l_v2ScanStep)
			{
				FretBuzzSystem::PixelColor l_CurrentColor = m_pSpriteColor[int(l_v2Start.Y + 0.5f) * m_iWidth + (unsigned int)(l_v2Start.X + 0.5f)];
				if (l_CurrentColor != m_ColorKey)
				{
					a_GFX.DrawPixel(l_iXIndex, l_iYIndex, l_CurrentColor);
				}
			}
		}
	}

	void Tex2D::DrawFlatBottomTriangle(FretBuzzMath::Vertex2D a_vert0, FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		int l_iYStart = max((int)ceil(a_vert0.m_v2Model.Y), a_Rect->m_fTop);
		int l_iYEnd = min((int)ceil(a_vert2.m_v2Model.Y) - 1, a_Rect->m_fBottom);

		float l_fLeftSlope = (a_vert1.m_v2Model.X - a_vert0.m_v2Model.X) / (a_vert1.m_v2Model.Y - a_vert0.m_v2Model.Y);
		float l_fRightSlope = (a_vert2.m_v2Model.X - a_vert0.m_v2Model.X) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y);

		FretBuzzMath::Vec2 l_v2LeftTextureSingleStep = (a_vert1.m_v2Texture - a_vert0.m_v2Texture) / (a_vert1.m_v2Model.Y - a_vert0.m_v2Model.Y);
		FretBuzzMath::Vec2 l_v2RightTextureSingleStep = (a_vert2.m_v2Texture - a_vert0.m_v2Texture) / (a_vert2.m_v2Model.Y - a_vert0.m_v2Model.Y);

		FretBuzzMath::Vec2 l_v2LeftPrestep = a_vert0.m_v2Texture + l_v2LeftTextureSingleStep * (l_iYStart - a_vert0.m_v2Model.Y);
		FretBuzzMath::Vec2 l_v2RightPrestep = a_vert0.m_v2Texture + l_v2RightTextureSingleStep * (l_iYStart - a_vert0.m_v2Model.Y);

		for (int l_iYIndex = l_iYStart; l_iYIndex <= l_iYEnd; l_iYIndex++,
			l_v2LeftPrestep += l_v2LeftTextureSingleStep, l_v2RightPrestep += l_v2RightTextureSingleStep)
		{
			float l_fXLeft = l_fLeftSlope * ((float)l_iYIndex - a_vert0.m_v2Model.Y) + a_vert0.m_v2Model.X;
			float l_fXRight = l_fRightSlope * ((float)l_iYIndex - a_vert0.m_v2Model.Y) + a_vert0.m_v2Model.X;

			int l_iXStart = max((int)ceil(l_fXLeft), a_Rect->m_fLeft);
			int l_iXEnd = min((int)ceil(l_fXRight) - 1, a_Rect->m_fRight);

			FretBuzzMath::Vec2 l_v2ScanStep = (l_v2RightPrestep - l_v2LeftPrestep) / (l_fXRight - l_fXLeft);
			FretBuzzMath::Vec2 l_v2Start = l_v2LeftPrestep + l_v2ScanStep * (l_iXStart - l_fXLeft);

			for (int l_iXIndex = l_iXStart; l_iXIndex <= l_iXEnd; l_iXIndex++, l_v2Start += l_v2ScanStep)
			{
				FretBuzzSystem::PixelColor l_CurrentColor = m_pSpriteColor[int(l_v2Start.Y + 0.5f) * m_iWidth + (unsigned int)(l_v2Start.X + 0.5f)];

				if(l_CurrentColor != m_ColorKey)
				{
					a_GFX.DrawPixel(l_iXIndex, l_iYIndex, l_CurrentColor);
				}
			}
		}
	}

}