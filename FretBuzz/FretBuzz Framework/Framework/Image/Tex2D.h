#pragma once
#include "Sprite.h"
#include "..\Math\Vertex2D.h"
#include <array>

namespace FretBuzzEngine
{
	class Tex2D : public Sprite
	{
	public:
		Tex2D(const std::string& a_strFileName, bool a_bIsContainsColorKey = false, FretBuzzSystem::PixelColor a_ColorKey = NULL);
		Tex2D(const Sprite* const a_pSpriteSheet, int a_iXSpriteSheetPosition, int a_iYSpriteSheetPosition, int a_iSpriteWidth, int a_iSpriteHeight);

		~Tex2D();

		virtual void DrawSprite(FretBuzzMath::Mat3& a_m3Transform, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect) override;

	private:
		std::array<FretBuzzMath::Vertex2D, 4> m_arrVertex;
		std::array<FretBuzzMath::Vec2, 4> m_arrV2StartVecPos;

		void Init();

		void DrawTexTriangle(FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzMath::Vertex2D a_vert3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);

		void DrawFlatTopTriangle(FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzMath::Vertex2D a_vert3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);
		void DrawFlatBottomTriangle(FretBuzzMath::Vertex2D a_vert1, FretBuzzMath::Vertex2D a_vert2, FretBuzzMath::Vertex2D a_vert3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);
	};
}