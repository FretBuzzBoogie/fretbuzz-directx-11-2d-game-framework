#pragma once
#include "SpriteAnimation.h"
#include <sstream>

namespace FretBuzzEngine
{
	//Loads the sprite animation with spritefilename[index] where index = 0 - num of elements
	//The time per frame states how much time each frame of the animation should be displayed
	SpriteAnimation::SpriteAnimation(unsigned int a_uiNumOfSprites, const std::string& a_strAnimationBaseName, float a_fTimePerPrame, FretBuzzEngine::InvertType a_InvertType, bool a_bIsContainColorKey
		, FretBuzzSystem::PixelColor a_ColorKey)
		: m_iNumOfSprites{ a_uiNumOfSprites },
		m_strAnimationBaseName{ a_strAnimationBaseName },
		m_fTimePerFrame{ a_fTimePerPrame },
		m_bIsContainColorKey{ a_bIsContainColorKey },
		m_ColorKey{a_ColorKey}
	{
		m_pSpriteList = new Sprite*[m_iNumOfSprites];

		constexpr int EXTENSION_LENGTH = 4;
		const unsigned int EXTENSION_START = m_strAnimationBaseName.length() - EXTENSION_LENGTH;
		const std::string STR_EXTENSION = m_strAnimationBaseName.substr(EXTENSION_START);
		const std::string STR_BASENAME = m_strAnimationBaseName.substr(0, EXTENSION_START);

		for (int l_iSpriteIndex = 0; l_iSpriteIndex < m_iNumOfSprites; l_iSpriteIndex++)
		{
			std::string l_strSprite = STR_BASENAME + std::to_string(l_iSpriteIndex) + STR_EXTENSION;
			m_pSpriteList[l_iSpriteIndex] = new Sprite(l_strSprite, a_InvertType, m_bIsContainColorKey, m_ColorKey);
		}

		m_pCurrentSprite = m_pSpriteList[0];
	}

	SpriteAnimation::SpriteAnimation(Spritesheet* a_pSpriteSheet, float a_fTimePerPrame)
		:m_iNumOfSprites{a_pSpriteSheet->m_iNoOfSprites },
		m_fTimePerFrame{ a_fTimePerPrame }
	{
		m_pSpriteList = a_pSpriteSheet->m_ppSprites;
		m_bIsSpritesLoadedExternally = true;
		m_pCurrentSprite = m_pSpriteList[0];
	}

	//Deletes the sprite list
	SpriteAnimation::~SpriteAnimation()
	{
		if (!m_bIsSpritesLoadedExternally)
		{
			for (int l_iSpriteIndex = 0; l_iSpriteIndex < m_iNumOfSprites; m_iNumOfSprites++)
			{
				delete m_pSpriteList[l_iSpriteIndex];
				m_pSpriteList[l_iSpriteIndex] = nullptr;
			}
			delete[] m_pSpriteList;
			m_pSpriteList = nullptr;
		}
	}

	//Draws the animation at given poisition
	void SpriteAnimation::DrawAnimation(int x, int y, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_pRect)
	{
		m_pCurrentSprite->DrawSprite(x, y, a_GFX, a_pRect);
	}

	//Draws the animation at given poisition
	void SpriteAnimation::DrawAnimation(FretBuzzMath::Mat3& a_Mat3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_pRect)
	{
		m_pCurrentSprite->DrawSprite(a_Mat3, a_GFX, a_pRect);
	}

	//Called when the current Sprite animation is going on to change the current sprite
	//It is not called via the IUpdateTimer because a character could have multiple SpriteAnimations
	//Only one animation would be running at a time
	void SpriteAnimation::OnUpdateAnimation(const float& a_fDeltaTime)
	{
		m_fTimePassedInCurrFrame += a_fDeltaTime;
		if (m_fTimePassedInCurrFrame >= m_fTimePerFrame)
		{
			m_iCurrentSpriteIndex++;
			m_iCurrentSpriteIndex %= m_iNumOfSprites;
			m_pCurrentSprite = m_pSpriteList[m_iCurrentSpriteIndex];
			m_fTimePassedInCurrFrame = 0.0f;
		}
	}
}