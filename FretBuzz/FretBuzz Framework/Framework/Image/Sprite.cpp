#pragma once
#include "Sprite.h"
#include "ImageLoader.h"
#include <string>
#include <fstream>
#include <exception>
#include "../Debugging/Debug.h"

namespace FretBuzzEngine
{
	//Constructor that loads the bitmp file or PNG file and specifies other details 
	//how to load the image, loads into bitmap
	//Specify the type of loader, if not it will load using the last set Loader
	//into the pointer PixelColor*
	Sprite::Sprite(const std::string& a_strFileName, bool a_bIsContainsColorKey, FretBuzzSystem::PixelColor a_ColorKey)
		: m_ColorKey(a_ColorKey),
		m_bIsContainsColorKey{ a_bIsContainsColorKey }
	{
		ImageLoader::LoadSprite(a_strFileName, m_pSpriteColor, m_iWidth, m_iHeight);
	}

	//Constructor that is similar loading the sprite but takes an invert arguement
	//the invert arguement specifies which orientation to invert the image
	Sprite::Sprite(const std::string& a_strFileName, InvertType a_enInvertType, bool a_bIsContainsColorKey, FretBuzzSystem::PixelColor a_ColorKey)
		: Sprite(a_strFileName, a_bIsContainsColorKey, a_ColorKey)
	{

		switch (a_enInvertType)
		{
			case InvertType::INVERT_HORIZONTAL:
			{
				InvertHorizontal();
				break;
			}
			case InvertType::INVERT_VERTICAL:
			{
				InvertVertical();
				break;
			}
			case InvertType::INVERT_VERT_AND_HORIZ:
			{
				InvertHorizontal();
				InvertVertical();
				break;
			}
			default:
			{
				break;
			}
		}
	}

	Sprite::Sprite(const Sprite* const a_pSpriteSheet, int a_iXSpriteSheetPosition, int a_iYSpriteSheetPosition, int a_iSpriteWidth, int a_iSpriteHeight)
		: m_iWidth { (unsigned int)a_iSpriteWidth },
		m_iHeight{(unsigned int) a_iSpriteHeight },
		m_ColorKey(a_pSpriteSheet->m_ColorKey),
		m_bIsContainsColorKey{ a_pSpriteSheet->m_bIsContainsColorKey}
	{
		m_pSpriteColor = new FretBuzzSystem::PixelColor[a_iSpriteHeight * a_iSpriteWidth];

		int l_iYMaxInSpriteSheet = a_iYSpriteSheetPosition + a_iSpriteHeight;
		int l_iXMaxInSpriteSheet = a_iXSpriteSheetPosition + a_iSpriteWidth;

		int l_iPixelIndex = 0;
		for (int l_iYIndex = a_iYSpriteSheetPosition; l_iYIndex < l_iYMaxInSpriteSheet; l_iYIndex++)
		{
			int l_iCurrentStartOfRow = l_iYIndex * a_pSpriteSheet->m_iWidth;
			for (int l_iXIndex = a_iXSpriteSheetPosition; l_iXIndex < l_iXMaxInSpriteSheet; l_iXIndex++, l_iPixelIndex++)
			{
				m_pSpriteColor[l_iPixelIndex] = a_pSpriteSheet->m_pSpriteColor[l_iCurrentStartOfRow + l_iXIndex];
			}
		}
	}

	//Destroys the loaded sprite
	Sprite::~Sprite()
	{
		if (m_pSpriteColor != nullptr)
		{
			delete[] m_pSpriteColor;
			m_pSpriteColor = nullptr;
		}
	}

	//Inverts the pixel array horizontally
	void Sprite::InvertHorizontal()
	{
		for (int l_iY = 0; l_iY < m_iHeight; l_iY++)
		{
			int l_iStartX = l_iY * m_iWidth;
			int l_iEndX = (l_iY * m_iWidth) + m_iWidth - 1;

			for (; !(l_iStartX >= l_iEndX); l_iStartX++, l_iEndX--)
			{
				FretBuzzSystem::PixelColor l_Color = m_pSpriteColor[l_iStartX];
				m_pSpriteColor[l_iStartX] = m_pSpriteColor[l_iEndX];
				m_pSpriteColor[l_iEndX] = l_Color;
			}
		}
	}

	//Inverts the pixel array vertically
	void Sprite::InvertVertical()
	{
		int l_iStartY = 0;
		int l_iEndY = m_iHeight - 1;

		for (; !(l_iStartY >= l_iEndY); l_iStartY++, l_iEndY--)
		{
			for (int l_iXIndex = 0; l_iXIndex < m_iWidth; l_iXIndex++)
			{
				int l_iLeft = l_iStartY * m_iWidth + l_iXIndex;
				int l_iRight = l_iEndY * m_iWidth + l_iXIndex;

				FretBuzzSystem::PixelColor l_Color = m_pSpriteColor[l_iLeft];
				m_pSpriteColor[l_iLeft] = m_pSpriteColor[l_iRight];
				m_pSpriteColor[l_iRight] = l_Color;
			}
		}
	}

	//Draws the sprite at position
	void Sprite::DrawSprite(int a_iX, int a_iY, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{

		int l_iTop = fmax(a_Rect->m_fTop - a_iY, 0.0f);
		int l_iBottom = fmin(m_iHeight,  - a_iY + a_Rect->m_fBottom);
		int l_iLeft = fmax(a_Rect->m_fLeft - a_iX, 0.0f);
		int l_iRight = fmin(m_iWidth, - a_iX + a_Rect->m_fRight);

		FretBuzzSystem::PixelColor l_CurrentColor;

		for (int l_iYIndex = l_iTop; l_iYIndex < l_iBottom; l_iYIndex++)
		{
			for (int l_iXIndex = l_iLeft; l_iXIndex < l_iRight; l_iXIndex++)
			{
				l_CurrentColor = m_pSpriteColor[l_iYIndex * m_iWidth + l_iXIndex];

				if (m_bIsContainsColorKey && (l_CurrentColor == m_ColorKey))
				{
					continue;
				}
				a_GFX.DrawPixel(l_iXIndex + a_iX,l_iYIndex + a_iY, l_CurrentColor);
			}
		}
	}

	void Sprite::DrawSprite(FretBuzzMath::Mat3& a_Mat3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		
	}

	//Returns the width of the sprite
	int Sprite::GetWidth() const
	{
		return m_iWidth;
	}

	//Returns the height of the sprite
	int Sprite::GetHeight() const
	{
		return m_iHeight;
	}

}