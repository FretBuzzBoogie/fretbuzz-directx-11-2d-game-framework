#pragma once
#include "../Graphics/PixelColor.h"
#include <string>
#include "Sprite.h"
#include "../Libs/LodePNG/lodepng.h"

namespace FretBuzzEngine
{
	//This class consists of static public functions to
	//load an image which is a bitmap or png
	struct ImageLoader
	{
	private:
		static void LoadBMP(std::ifstream& a_ImageFileStream, FretBuzzSystem::PixelColor*& a_pPixelColor, unsigned int& a_iWidth, unsigned int& a_iHeight);
		static void LoadPNG(std::string& const a_strFilePath, FretBuzzSystem::PixelColor*& a_pPixelColor, const int& a_iBitDepth, const LodePNGColorType a_PNGColorType,
			unsigned int& a_iWidth, unsigned int& a_iHeight);

	public:
		static void LoadSprite(const std::string& a_strFileName, FretBuzzSystem::PixelColor*& a_pPixelColor, unsigned int& a_iWidth, unsigned int& a_iHeight);

		static const std::string STR_RESOURCES_DIRECTORY;
		static const std::string STR_EXTENSION_PNG;
		static const std::string STR_EXTENSION_BMP;
		static constexpr int EXTENSION_CHAR_LENGTH = 4;
	};
}