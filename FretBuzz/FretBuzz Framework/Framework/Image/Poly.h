#pragma once
#include "../Libs/dxflib/dl_creationadapter.h"
#include <string>
#include "../Math/Vec2.h"
#include "../Graphics/Graphics.h"
#include <memory>

namespace FretBuzzEngine
{
	class Poly : public DL_CreationAdapter
	{
	public:
		Poly(const std::string& a_strDXFFileName);
		~Poly();

		void DrawPoly(FretBuzzMath::Vec2 a_Position, FretBuzzSystem::Graphics& a_GFX , FretBuzzSystem::PixelColor a_PixelColor);

	protected:
		virtual void addVertex(const DL_VertexData& data) override;

	private:
		std::vector<FretBuzzMath::Vec2> m_VertexVect;
		const static std::string DXF_FOLDER_PATH;
	};
}