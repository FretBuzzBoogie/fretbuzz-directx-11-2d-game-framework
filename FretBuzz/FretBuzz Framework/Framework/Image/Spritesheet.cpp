#include "Spritesheet.h"
#include "Tex2D.h"

namespace FretBuzzEngine
{
	Spritesheet::Spritesheet(std::string a_strSpriteSheetPath, unsigned int a_iNoOfSprites, unsigned int a_iSpritesPerRow, unsigned int a_iSpritesPerCol, bool a_bIsTex2D, bool a_bisContainsColorKey, FretBuzzSystem::PixelColor a_ColorKey)
		: m_iNoOfSprites{ a_iNoOfSprites },
		m_iSpritesPerCol{ a_iSpritesPerCol },
		m_iSpritesPerRow{ a_iSpritesPerRow }
	{
		m_pSpriteSheet = new Sprite(a_strSpriteSheetPath, a_bisContainsColorKey, a_ColorKey);
		m_iSpriteSheetHeight = m_pSpriteSheet->GetHeight();
		m_iSpriteSheetWidth = m_pSpriteSheet->GetWidth();

		m_iSingleSpriteWidth = m_iSpriteSheetWidth / m_iSpritesPerRow;
		m_iSingleSpriteHeight = m_iSpriteSheetHeight / m_iSpritesPerCol;

		m_ppSprites = a_bIsTex2D ? (Sprite**)(new Tex2D*[m_iNoOfSprites]) : new Sprite*[m_iNoOfSprites];

		if (a_bIsTex2D)
		{
			for (int l_iSpriteindex = 0; l_iSpriteindex < m_iNoOfSprites; l_iSpriteindex++)
			{
				m_ppSprites[l_iSpriteindex] = new Tex2D(m_pSpriteSheet, (l_iSpriteindex % m_iSpritesPerRow) * m_iSingleSpriteWidth, (l_iSpriteindex / m_iSpritesPerRow) * m_iSingleSpriteHeight, m_iSingleSpriteWidth, m_iSpriteSheetHeight);
			}
		}
		else
		{
			for (int l_iSpriteindex = 0; l_iSpriteindex < m_iNoOfSprites; l_iSpriteindex++)
			{
				m_ppSprites[l_iSpriteindex] = new Sprite(m_pSpriteSheet, (l_iSpriteindex % m_iSpritesPerRow) * m_iSingleSpriteWidth, (l_iSpriteindex / m_iSpritesPerRow) * m_iSingleSpriteHeight, m_iSingleSpriteWidth, m_iSpriteSheetHeight);
			}
		}
	}

	Spritesheet::~Spritesheet()
	{
		for (int l_iSpriteindex = 0; l_iSpriteindex < m_iNoOfSprites; l_iSpriteindex++)
		{
			delete m_ppSprites[l_iSpriteindex];
			m_ppSprites[l_iSpriteindex] = nullptr;
		}
		delete[] m_ppSprites;
		m_ppSprites = nullptr;

		delete m_pSpriteSheet;
		m_pSpriteSheet = nullptr;
	}

	unsigned  Spritesheet::GetTotalSpritesInSheet() const
	{
		return m_iNoOfSprites;
	}

	unsigned  Spritesheet::GetSingleSpriteWidth() const
	{
		return m_iSingleSpriteWidth;
	}

	unsigned  Spritesheet::GetSingleSpriteHeight() const
	{
		return m_iSingleSpriteHeight;
	}

	unsigned  Spritesheet::GetSpriteSheetWidth() const
	{
		return m_iSingleSpriteWidth;
	}

	unsigned  Spritesheet::GetSpriteSheetHeight() const
	{
		return m_iSpriteSheetHeight;
	}

	void Spritesheet::DrawSpriteSheet(int a_iX, int a_iY, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		m_pSpriteSheet->DrawSprite(a_iX, a_iY, a_GFX, a_Rect);
	}

	void Spritesheet::DrawSpriteIndex(int a_iX, int a_iY, int a_iSpriteIndex, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect)
	{
		m_ppSprites[a_iSpriteIndex]->DrawSprite(a_iX, a_iY, a_GFX, a_Rect);
	}
}