#pragma once
#include <string>
#include "Sprite.h"

namespace FretBuzzEngine
{
	class Spritesheet
	{
	public:
		friend class SpriteAnimation;

		Spritesheet(std::string a_strSpriteSheetPath, unsigned int a_iNoOfSprites, unsigned int a_iSpritesPerRow, unsigned int a_iSpritesPerCol , bool a_bIsTex2D = false, bool a_bisContainsColorKey = false, FretBuzzSystem::PixelColor a_ColorKey = 0);
		~Spritesheet();

		unsigned int GetTotalSpritesInSheet() const;
		unsigned int GetSingleSpriteWidth() const;
		unsigned int GetSingleSpriteHeight() const;

		unsigned int GetSpriteSheetWidth() const;
		unsigned int GetSpriteSheetHeight() const;

		void DrawSpriteSheet(int a_iX, int a_iY, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);
		void DrawSpriteIndex(int a_iX, int a_iY, int a_iSpriteIndex, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_Rect);

	private:
		Sprite* m_pSpriteSheet = nullptr;
		Sprite** m_ppSprites = nullptr;

		unsigned int m_iNoOfSprites = 0;
		unsigned  int m_iSpritesPerCol = 0;
		unsigned int m_iSpritesPerRow = 0;
		unsigned int m_iSingleSpriteWidth = 0;
		unsigned int m_iSingleSpriteHeight = 0;

		unsigned int m_iSpriteSheetWidth = 0;
		unsigned int m_iSpriteSheetHeight = 0;
	};
}