#pragma once
#include "../Graphics/Graphics.h"
#include "Sprite.h"
#include "Spritesheet.h"

namespace FretBuzzEngine
{
	class SpriteAnimation
	{
	private:
		unsigned int m_iNumOfSprites = 0;
		float m_fTimePerFrame = 0.0f;
		float m_fTimePassedInCurrFrame = 0.0f;
		int m_iCurrentSpriteIndex = 0;

		std::string m_strAnimationBaseName;
		Sprite** m_pSpriteList = nullptr;
		Sprite* m_pCurrentSprite = nullptr;

		bool m_bIsContainColorKey = false;
		FretBuzzSystem::PixelColor m_ColorKey;

		bool m_bIsSpritesLoadedExternally = false;

	public:
		SpriteAnimation(unsigned int a_uiNumOfSprites, const std::string& a_strAnimationBaseName, float a_fTimePerPrame, 
			FretBuzzEngine::InvertType a_InvertType = InvertType::DONT_INVERT , bool a_bIsContainColorKey = false, FretBuzzSystem::PixelColor a_ColorKey = NULL);
		SpriteAnimation(Spritesheet* a_pSpriteSheet, float a_fTimePerPrame);


		~SpriteAnimation();

		void OnUpdateAnimation(const float& a_fDeltaTime);
		void DrawAnimation(int x, int y, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_pRect = nullptr);
		void DrawAnimation(FretBuzzMath::Mat3& a_Mat3, FretBuzzSystem::Graphics& a_GFX, FretBuzzMath::Rect* a_pRect = nullptr);
	};
}