#pragma once
#include "Graphics.h"
#include <functional>
#include "../../Game/Game.h"

namespace FretBuzzSystem
{
	//Singleton instance
	Graphics* Graphics::s_pInstance = nullptr;

	//Constructor called only once in the system
	Graphics::Graphics()
	{
		const unsigned int l_uiBackgroundColor = 0x00;

		int l_iTotalPixels = System::s_uiScreenHeight * System::s_uiScreenWidth;
		m_pPixelColor = new PixelColor[l_iTotalPixels];
		m_pBackgroundColor = new PixelColor[l_iTotalPixels];

		m_pTimer = Timer::GetInstance();
		m_pGame = new Game(this);

		//initializes background color array
		for (int l_iColorIndex = 0; l_iColorIndex < l_iTotalPixels; l_iColorIndex++)
		{
			m_pBackgroundColor[l_iColorIndex] = l_uiBackgroundColor;
		}
	}

	//Destructor
	Graphics::~Graphics()
	{
		//Deletes the background color array
		if (m_pBackgroundColor != nullptr)
		{
			delete[] m_pBackgroundColor;
			m_pBackgroundColor = nullptr;
		}

		//Deletes the pixel color i.e. the main buffer that stores the pixel color
		if (m_pPixelColor != nullptr)
		{
			delete[] s_pInstance->m_pPixelColor;
			s_pInstance->m_pPixelColor = nullptr;
		}

		if (m_pGame != nullptr)
		{
			delete m_pGame;
			m_pGame = nullptr;
		}

		m_pTimer->Destroy();
	}

	//Returns the instance
	//Creates a new instance if not already created
	Graphics* const Graphics::GetInstance(const bool a_bIsFullScreen)
	{
		if (s_pInstance == nullptr)
		{
			s_pInstance = new Graphics();
			s_pInstance->m_pD3D = Direct3D::GetInstance(System::GetScreenWidth(), System::GetScreenHeight(),
				IS_V_SYNC_ENABLED,System::GetWindowhandle(), a_bIsFullScreen,SCREEN_DEPTH, SCREEN_NEAR, s_pInstance->m_pPixelColor, s_pInstance->m_pBackgroundColor);

			if (s_pInstance->m_pD3D == nullptr)
			{
				s_pInstance->Destroy();
				return nullptr;
			}
		}
		return s_pInstance;
	}

	//Destroys the singleton instance if exists
	void Graphics::Destroy()
	{
		if (s_pInstance != nullptr)
		{
			delete s_pInstance;
			s_pInstance = nullptr;
		}
	}

	//Renders on the screen
	void Graphics::Render()
	{
		m_pD3D->BeginFrame();
		
		m_pTimer->FrameUpdate(std::chrono::steady_clock::now());
	
		m_pTimer->FrameLateUpdate();

		m_pD3D->EndFrame();
	}

	//Draws the pixel at given poition with given color
	void Graphics::DrawPixel(const int& X, const int& Y,
		const int& a_iRed, const int& a_iGreen, const int& a_iBlue)
	{
		DrawPixel(X, Y, PixelColor(a_iRed, a_iGreen, a_iBlue, 255));
	}

	//Draws the pixel at given poition with given color
	void Graphics::DrawPixel(const int& X, const int& Y, PixelColor& a_Color)
	{
		m_pPixelColor[Y * System::s_uiScreenWidth + X] = a_Color;
	}

	//Using Sutherland - hodgman for line clipping
	//Uses the min screen X and Y and max screen X and Y as default
	//Returns if the line is accepted to draw the line as it is positioned within the Window rect
	bool Graphics::GetClippedLine(float& a_fFromX, float& a_fFromY, float& a_fToX, float& a_fToY,
		const float a_fMinX, const float a_fMinY, const float a_fMaxX, const float a_fMaxY)
	{
		const int INSIDE_CODE	=	0x0000;
		const int TOP_CODE		=	0x1000;
		const int BOTTOM_CODE	=	0x0100;
		const int LEFT_CODE		=	0x0010;
		const int RIGHT_CODE	=	0x0001;

		std::function<int(int, int)> GetCode =
		[=](int x, int y) -> int
		{
			int l_iResultOutCode = 0x0000;
			if (x < a_fMinX)
			{
				l_iResultOutCode |= LEFT_CODE;
			}
			else if (x > a_fMaxX)
			{
				l_iResultOutCode |= RIGHT_CODE;
			}

			if (y < a_fMinY)
			{
				l_iResultOutCode |= TOP_CODE;
			}
			else if (y > a_fMaxY)
			{
				l_iResultOutCode |= BOTTOM_CODE;
			}

			return l_iResultOutCode;
		};

		int OutCode1 = GetCode(a_fFromX, a_fFromY);
		int OutCode2 = GetCode(a_fToX, a_fToY);
		bool l_bIsAccepted = false;

		while (true)
		{
			if (!(OutCode1 | OutCode2))
			{
				l_bIsAccepted = true;
				break;
			}
			else if(OutCode1 & OutCode2)
			{
				//Not accepted, both the points are outside the rect
				break;
			}
			else
			{
				int l_iCurrOutCode = OutCode1 ? OutCode1 : OutCode2;
				float X = 0;
				float Y = 0;

				if (l_iCurrOutCode & TOP_CODE)
				{
					X = ((a_fToX - a_fFromX) / (a_fToY - a_fFromY)) * (a_fMinY - a_fFromY) + a_fFromX;
					Y = a_fMinY;
				}
				else if (l_iCurrOutCode & BOTTOM_CODE)
				{
					X = ((a_fToX - a_fFromX) / (a_fToY - a_fFromY)) * (a_fMaxY - a_fFromY) + a_fFromX;
					Y = a_fMaxY;
				}
				else if (l_iCurrOutCode & LEFT_CODE)
				{
					Y = ((a_fToY - a_fFromY) / (a_fToX - a_fFromX)) * (a_fMinX - a_fFromX) + a_fFromY;
					X = a_fMinX;
				}
				else if (l_iCurrOutCode & RIGHT_CODE)
				{
					Y = (((a_fToY - a_fFromY) / (a_fToX - a_fFromX)) * (a_fMaxX - a_fFromX)) + a_fFromY;
					X = a_fMaxX;
				}

				if (l_iCurrOutCode == OutCode1)
				{
					a_fFromX = X;
					a_fFromY = Y;
					OutCode1 = GetCode(X, Y);
				}
				else
				{
					a_fToX = X;
					a_fToY = Y;
					OutCode2 = GetCode(X, Y);
				}
			}
		}
			return l_bIsAccepted;
	}


	//Draws a line from 1 point to another
	void Graphics::DrawLine(float X1, float Y1, float X2, float Y2, PixelColor& a_Color)
	{
		if (!GetClippedLine(X1, Y1, X2, Y2))
		{
			return;
		}

		float l_fDeltaY = (Y2 - Y1);
		float l_fDeltaX = (X2 - X1);

		if (abs(l_fDeltaX) > abs(l_fDeltaY))
		{
			float l_fSlope = (l_fDeltaY / l_fDeltaX);
			int l_iYIntercept = Y1 - l_fSlope * X1;

			if (X1 > X2)
			{
				int l_iTemp = X2;
				X2 = X1;
				X1 = l_iTemp;
			}

			int l_iYIndex = 0;
			for (int l_iXIndex = X1; l_iXIndex <= X2; l_iXIndex++)
			{
				l_iYIndex = l_fSlope * l_iXIndex + l_iYIntercept + 0.5f;
				DrawPixel(l_iXIndex, l_iYIndex, a_Color);
			}
		}
		else
		{
			float l_fSlope = (l_fDeltaX / l_fDeltaY);
			int l_iXIntercept = X1 - l_fSlope * Y1;

			if (Y1 > Y2)
			{
				int l_iTemp = Y2;
				Y2 = Y1;
				Y1 = l_iTemp;
			}

			int l_iXIndex = 0;
			for (int l_iYIndex = Y1; l_iYIndex <= Y2; l_iYIndex++)
			{
				l_iXIndex = l_fSlope * l_iYIndex + l_iXIntercept + 0.5f;
				DrawPixel(l_iXIndex, l_iYIndex, a_Color);
			}
		}
	}

	//Draws a line from a Vec2 to another Vec2
	void Graphics::DrawLine(const FretBuzzMath::Vec2& a_Vec2From, const FretBuzzMath::Vec2& a_Vec2To, PixelColor& a_Color)
	{
		DrawLine(a_Vec2From.X, a_Vec2From.Y, a_Vec2To.X, a_Vec2To.Y, a_Color);
	}
}