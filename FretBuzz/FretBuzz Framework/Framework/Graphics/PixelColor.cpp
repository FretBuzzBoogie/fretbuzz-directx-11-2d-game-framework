#include "PixelColor.h"
#pragma once

namespace FretBuzzSystem
{
	PixelColor::PixelColor(const PixelColor& a_PixelColor)
	{
		this->m_uiColor = a_PixelColor.m_uiColor;
	}

	PixelColor::PixelColor(const unsigned int a_uiRed, const unsigned int a_uiGreen, const unsigned int a_uiBlue, const unsigned int a_uiAlpha)
		:m_uiColor(a_uiAlpha << 24 | a_uiRed << 16 | a_uiGreen << 8 | a_uiBlue)
	{
	}

	///m_uiColor is stored in the format A8R8G8B8
	PixelColor::PixelColor(const unsigned int a_uiColor)
	{
		m_uiColor = a_uiColor;
	}

	PixelColor::~PixelColor()
	{
	}

	void PixelColor::operator=(const PixelColor& a_Color)
	{
		 m_uiColor = a_Color.m_uiColor;
	}

	void PixelColor::operator=(const unsigned int a_uiColor)
	{
		m_uiColor = a_uiColor;
	}

	bool PixelColor::operator==(const PixelColor& a_PixelColor)
	{
		return this->m_uiColor == a_PixelColor.m_uiColor;
	}

	bool PixelColor::operator!=(const PixelColor & a_PixelColor)
	{
		return this->m_uiColor != a_PixelColor.m_uiColor;
	}

	void PixelColor::SetAlpha(const unsigned int a_uiAlpha)
	{
		m_uiColor = (m_uiColor & 0x00FFFFFF) | (a_uiAlpha << 24);
	}

	void PixelColor::SetRed(const unsigned int a_uiRed)
	{
		m_uiColor = (m_uiColor & 0xFF00FFFF ) | (a_uiRed << 16);
	}

	void PixelColor::SetGreen(const unsigned int a_uiGreen)
	{
		m_uiColor = (m_uiColor & 0xFFFF00FF) | (a_uiGreen << 8);
	}

	void PixelColor::SetBlue(const unsigned int a_uiBlue)
	{
		m_uiColor = (m_uiColor & 0xFFFFFF00) | (a_uiBlue);
	}

	unsigned int PixelColor::GetAlpha()
	{
		return m_uiColor >> 24;
	}

	unsigned int PixelColor::GetRed()
	{
		return (m_uiColor >> 16) & 0xFF;
	}

	unsigned int PixelColor::GetGreen()
	{
		return (m_uiColor >> 8) & 0xFF;
	}

	unsigned int PixelColor::GetBlue()
	{
		return m_uiColor & 0xFF;
	}


	//Calculates the alpha variable into the Red Green and Blue
	//This is used for Colors of type RGBA
	//The basic pixel color will have alpha 255
	//but in case of scenarios example loading a PNG of type RGBA we need to merge
	// the alpha value with the RGB values
	PixelColor& PixelColor:: CalculateAlphaIntoRGB()
	{
		const unsigned int l_iPixelAlpha = GetAlpha();
		if (l_iPixelAlpha == 255 || l_iPixelAlpha == 0)
		{
			return *this;
		}

		SetRed(GetRed() * (l_iPixelAlpha / 255));
		SetGreen(GetGreen() * (l_iPixelAlpha / 255));
		SetBlue(GetBlue() * (l_iPixelAlpha / 255));
		SetAlpha(255);

		return *this;
	}
}