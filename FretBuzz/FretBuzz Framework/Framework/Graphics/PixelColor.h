#pragma once

namespace FretBuzzSystem
{
	class PixelColor
	{
	private:

	public:
		unsigned int m_uiColor = 0;
		PixelColor() = default;
		PixelColor(const PixelColor& a_PixelColor);
		PixelColor(const unsigned int a_uiRed, const unsigned int a_uiGreen, const unsigned int a_uiBlue, const unsigned int a_uiAlpha);
		PixelColor(const unsigned int a_uiColor);
		~PixelColor();

		void operator=(const PixelColor& a_Color);
		void operator=(const unsigned int a_uiColor);

		bool operator==(const PixelColor& a_PixelColor);
		bool operator!=(const PixelColor& a_PixelColor);

		void SetAlpha(const unsigned int a_uiAlpha);
		void SetRed(const unsigned int a_uiRed);
		void SetGreen(const unsigned int a_uiGreen);
		void SetBlue(const unsigned int a_uiBlue);

		unsigned int GetAlpha();
		unsigned int GetRed();
		unsigned int GetGreen();
		unsigned int GetBlue();

		PixelColor& CalculateAlphaIntoRGB();
	};
}
