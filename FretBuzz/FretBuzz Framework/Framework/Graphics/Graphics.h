#pragma once
#include "../System/System.h"
#include "Direct3D.h"
#include "PixelColor.h"
#include "../Timer/Timer.h"
#include "../Math/Vec2.h"
#include "../Camera/Renderable.h"

namespace FretBuzzSystem
{
	extern constexpr bool IS_V_SYNC_ENABLED = false;

	extern constexpr float SCREEN_DEPTH = 1000.0f;

	extern constexpr float SCREEN_NEAR = 0.1f;

	class Game;

	class Graphics
	{
	private:
		static Graphics* s_pInstance;

		Graphics();
		~Graphics();

		Game* m_pGame = nullptr;
		Timer* m_pTimer = nullptr;
		
		float m_fDeltaTime = 0.0f;

		PixelColor* m_pPixelColor = nullptr;
		PixelColor* m_pBackgroundColor = nullptr;

		bool GetClippedLine(float& a_fFromX, float& a_fFromY, float& a_fToX, float& a_fToY,
			const float a_fMinX = 0, const float a_fMinY = 0, const float a_fMaxX = System::s_uiScreenWidth - 1, const float a_iMaxY = System::s_uiScreenHeight - 1);

	public:

		static Graphics* const GetInstance(const bool a_bIsFullScreen);
		static void Destroy();

		Direct3D* m_pD3D = nullptr;

		void Render();
		void DrawPixel(const int& X, const int& Y,
			const int& a_iRed, const int& a_iGreen, const int& a_Blue);

		void DrawPixel(const int& X, const int& Y, PixelColor& a_Color);
		void DrawLine(float X1, float Y1, float X2, float Y2, PixelColor& a_Color);
		void DrawLine(const FretBuzzMath::Vec2& a_Vec2From, const FretBuzzMath::Vec2& a_Vec2To, PixelColor& a_Color);
	};
}