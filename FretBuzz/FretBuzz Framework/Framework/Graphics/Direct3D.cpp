#pragma once
#include "Direct3D.h"

namespace FretBuzzSystem
{
#pragma region Shader Files
	#include "../BuildShader/fragShader.bcde"
	#include "../BuildShader/vertShader.bcde"
#pragma endregion Shader Files

	//Singleton instance
	Direct3D* Direct3D::s_pInstance = nullptr;

	//Constructor that initializes the background and current pixel color
	Direct3D::Direct3D(PixelColor*& const a_PixelColor, PixelColor*& const a_PixelColorBackground )
		: m_pPixelColor(a_PixelColor),
		m_pBackgroundColor(a_PixelColorBackground)
	{
		m_uiSizeOfSingleColor = sizeof(PixelColor);
		m_iTotalPixels = System::GetScreenHeight() * System::GetScreenWidth();
		m_iSizeOfPixelArray = m_uiSizeOfSingleColor * m_iTotalPixels;
	}

	//Deconstructs the class
	//Deletes all dynamic memory over here instead in the destroy method
	Direct3D::~Direct3D()
	{
	}

	//Initializs the D3D11 Api
	//If failed then return false and will eventually destroy the created data
	bool Direct3D::IsInitializedD3D(int a_iScreenWidth, int a_iScreenHeight, bool a_bVsync,
		HWND a_WindowHandle, bool a_bIsFullscreen, float a_fScreenDepth, float a_fScreenNear)
	{
		//Initialize swap chain
		DXGI_SWAP_CHAIN_DESC l_SwapChainDesc;
		ZeroMemory(&l_SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
		l_SwapChainDesc.BufferCount = 1;									//No of buffers in swap chain

		//DXGI is probably using Little - Endian
		l_SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;		//Format that will be used to display the pixels 
		l_SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;		//Describes how you want to use the back buffer	
		l_SwapChainDesc.OutputWindow = a_WindowHandle;						//The handle of the window in which the game would run
		l_SwapChainDesc.Windowed = !a_bIsFullscreen;						//Is the window a in full screen or windowed mode
		l_SwapChainDesc.SampleDesc.Count = 1;								//Describes number of multisamples per pixel
		l_SwapChainDesc.SampleDesc.Quality = 0;								//The quality of the image displayed. Range 0 to (ID3D10Device::CheckMultisampleQualityLevels - 1)
		l_SwapChainDesc.BufferDesc.RefreshRate.Numerator = 1;				//Lowest acceptable refresh rate
		l_SwapChainDesc.BufferDesc.RefreshRate.Denominator = 60;			//Highest acceptable refresh rate
		l_SwapChainDesc.BufferDesc.Height = a_iScreenHeight;		//Screen height buffer
		l_SwapChainDesc.BufferDesc.Width = a_iScreenWidth;		//Screen width buffer

		//Creates the swap chain
		D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr,
			0, nullptr, 0, D3D11_SDK_VERSION, &l_SwapChainDesc, &m_pDxgiSwapChain,
			&m_pID3D11Device, nullptr, &m_pID3D11DevContext);

		//Set render targets
		ID3D11Resource* l_pResource = nullptr;
		m_pDxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&l_pResource);
		m_pID3D11Device->CreateRenderTargetView(l_pResource, nullptr, &m_pRenderTargetView);
		l_pResource->Release();
		m_pID3D11DevContext->OMSetRenderTargets(1, &m_pRenderTargetView, NULL);

		//Set the viewport
		D3D11_VIEWPORT l_Viewport;
		ZeroMemory(&l_Viewport, sizeof(D3D11_VIEWPORT));
		l_Viewport.TopLeftX = 0;
		l_Viewport.TopLeftY = 0;
		l_Viewport.MinDepth = 0.0f;
		l_Viewport.MaxDepth = 1.0f;
		l_Viewport.Width = a_iScreenWidth;
		l_Viewport.Height = a_iScreenHeight;
		m_pID3D11DevContext->RSSetViewports(1, &l_Viewport);


		//Texture 2D description to set the texture to be render on
		D3D11_TEXTURE2D_DESC l_Texture2DDescription = {};
		ZeroMemory(&l_Texture2DDescription, sizeof(D3D11_TEXTURE2D_DESC));
		l_Texture2DDescription.Width = a_iScreenWidth;
		l_Texture2DDescription.Height = a_iScreenHeight;
		l_Texture2DDescription.Usage = D3D11_USAGE_DYNAMIC;								//How to write or read texture
		l_Texture2DDescription.BindFlags = D3D11_BIND_SHADER_RESOURCE;					//Bind to pipeline stages
		l_Texture2DDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;					//CPU access
		l_Texture2DDescription.Format = DXGI_FORMAT_B8G8R8A8_UNORM;						//Format of texture
		l_Texture2DDescription.SampleDesc.Count = 1;									//Multisampling value
		l_Texture2DDescription.SampleDesc.Quality = 0;									//Quality of image
		l_Texture2DDescription.ArraySize = 1;											//Number of textures in array
		l_Texture2DDescription.MiscFlags = 0;											//Other flags
		l_Texture2DDescription.MipLevels = 1;											//
		m_pID3D11Device->CreateTexture2D(&l_Texture2DDescription, nullptr, &m_pTex2D);	//

		//Determines how the resource is cast when read
		// The format of the data in the resource
		D3D11_SHADER_RESOURCE_VIEW_DESC l_ShaderResourceView = {};
		ZeroMemory(&l_ShaderResourceView, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		l_ShaderResourceView.Format = l_Texture2DDescription.Format;					//Format of tehe texture
		l_ShaderResourceView.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;				//Resource is a 2D texture
		l_ShaderResourceView.Texture2D.MipLevels = 1;									//Max mip levels of texture 2D
		m_pID3D11Device->CreateShaderResourceView(m_pTex2D,
			&l_ShaderResourceView, &m_pShaderResourceView);

		try
		{
			InitializeShaderPipeline();
			InitializeVertexBufferToGPU();
		}
		catch (std::exception& a_Exception)
		{
			a_Exception.what(); //TODO:: Need to add logs to debug.
			return false;
		}

		//Create sampler, determines how the texture is to be rendered via pixel shader
		D3D11_SAMPLER_DESC l_SamplerDescription = {};
		ZeroMemory(&l_SamplerDescription, sizeof(D3D11_SAMPLER_DESC));
		l_SamplerDescription.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;			//
		l_SamplerDescription.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;			//Clamps U between 0 - 1
		l_SamplerDescription.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;			//Clamps V between 0 - 1
		l_SamplerDescription.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;			//Clamps W between 0 - 1
		l_SamplerDescription.ComparisonFunc = D3D11_COMPARISON_NEVER;			//Never pass the comparision		
		l_SamplerDescription.MinLOD = 0;										//Min Mip Map range to access
		l_SamplerDescription.MaxLOD = D3D11_FLOAT32_MAX;						//Max Mip Map range to access
		m_pID3D11Device->CreateSamplerState(&l_SamplerDescription, &m_pSamplerState);

		return true;
	}

	//Creates the shader pipeline
	//Sets the input layout, sets how the vertex shader should perceives the vertex data array
	void Direct3D::InitializeShaderPipeline()
	{
		//ToDO :: use the below code to compile the shader
		//The problem previously encountered was that the .hlsl files were needed to be included
		//in the output directory as the program would compile the shaders at runtime
		//We will need to create a separate function to compile the Shaders at runtime using the below code

		/////////////////////////////////////////////////////////////////
		/*
		//Blob required to compile the shaders
		ID3D10Blob* l_pBlobVertShader = nullptr;
		ID3D10Blob* l_pBlobFragShader = nullptr;

		//Compiles fragment and vertex shader
		D3DX11CompileFromFile(L"vertShader.hlsl", 0, 0, "vertMain", "vs_5_0", 0, 0, 0, &l_pBlobVertShader, 0, 0);
		if (l_pBlobVertShader == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to compile vertex shader.");
		}

		D3DX11CompileFromFile(L"fragShader.hlsl", 0, 0, "fragMain", "ps_5_0", 0, 0, 0, &l_pBlobFragShader, 0, 0);
		if (l_pBlobFragShader == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to compile fragment shader.");
		}

		m_pID3D11Device->CreateVertexShader(l_pBlobVertShader->GetBufferPointer(), l_pBlobVertShader->GetBufferSize(), NULL, &m_pVertShader);
		if (m_pVertShader == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to create vertex shader.");
		}

		m_pID3D11Device->CreatePixelShader(l_pBlobFragShader->GetBufferPointer(), l_pBlobFragShader->GetBufferSize(), NULL, &m_pFragShader);
		if (m_pFragShader == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to create fragment shader.");
		}
		
		m_pID3D11DevContext->VSSetShader(m_pVertShader, 0, 0);
		m_pID3D11DevContext->PSSetShader(m_pFragShader, 0, 0);
		
		//Sets up how the vertex shader perceives the vertex data array
		D3D11_INPUT_ELEMENT_DESC l_arrInputLayout[] =
		{
			{ "VERT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "PIXELCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		
		m_pID3D11Device->CreateInputLayout(l_arrInputLayout, 2, FretBuzzShaders::vertShaderByteCode, sizeof(FretBuzzShaders::vertShaderByteCode), &m_pInputLayout);
		if (m_pInputLayout == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to create the input layout.");
		}
		
		m_pID3D11DevContext->IASetInputLayout(m_pInputLayout);
		l_pBlobVertShader->Release();
		l_pBlobFragShader->Release();
		*/
		/////////////////////////////////////////////////////////////////

		m_pID3D11Device->CreatePixelShader(fragShaderByteCode, sizeof(fragShaderByteCode), nullptr, &m_pFragShader);
		m_pID3D11Device->CreateVertexShader(vertShaderByteCode, sizeof(vertShaderByteCode), nullptr, &m_pVertShader);

		m_pID3D11DevContext->VSSetShader(m_pVertShader, 0, 0);
		m_pID3D11DevContext->PSSetShader(m_pFragShader, 0, 0);

		//Sets up how the vertex shader perceives the vertex data array
		D3D11_INPUT_ELEMENT_DESC l_arrInputLayout[] =
		{
			{ "VERT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "PIXELCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		m_pID3D11Device->CreateInputLayout(l_arrInputLayout, 2, vertShaderByteCode, sizeof(vertShaderByteCode), &m_pInputLayout);
		if (m_pInputLayout == nullptr)
		{
			throw std::exception("Direct3D::InitializeShaderPipeline:: Failed to create the input layout.");
		}

		m_pID3D11DevContext->IASetInputLayout(m_pInputLayout);
	}

	//Initializizes the vertex buffer for the shader that is mapped
	//The vertex data is set as 2 triangles
	//the vertex data is sent to the GPU
	void Direct3D::InitializeVertexBufferToGPU()
	{
		//Vertex array that sets the texture edge vertex coordinates
		//Sets the pixel coordinates
		const VERTEX_DATA l_arrVertexData[] =
		{
			{ -1.0f,  1.0f, 0.5f, 0.0f, 0.0f},
			{  1.0f,  1.0f, 0.5f, 1.0f, 0.0f},
			{  1.0f, -1.0f, 0.5f, 1.0f, 1.0f},
							  		 
			{ -1.0f,  1.0f, 0.5f, 0.0f, 0.0f },
			{  1.0f, -1.0f, 0.5f, 1.0f, 1.0f },
			{ -1.0f, -1.0f, 0.5f, 0.0f, 1.0f }
		};

		//The description of the buffer to be sent to the GPU
		D3D11_BUFFER_DESC l_BufferDesc = {};
		ZeroMemory(&l_BufferDesc, sizeof(D3D11_BUFFER_DESC));						
		l_BufferDesc.Usage = D3D11_USAGE_DEFAULT;			//Describes how data is to be loaded or written to
		l_BufferDesc.ByteWidth = sizeof(VERTEX_DATA) * 6;   //Buffer size
		l_BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;  //Gives identity to the buffer of its use in the pipeline
		l_BufferDesc.CPUAccessFlags = 0;
		l_BufferDesc.MiscFlags = 0;

		//The subresource to set the Vertex Buffer with the vertex data
		D3D11_SUBRESOURCE_DATA l_SubResourceData = {};
		ZeroMemory(&l_SubResourceData, sizeof(D3D11_SUBRESOURCE_DATA));
		l_SubResourceData.pSysMem = l_arrVertexData;										//Points to the initialized data
		m_pID3D11Device->CreateBuffer(&l_BufferDesc, &l_SubResourceData, &m_pVertBuffer);	//Creates buffer with the data
	}

	//Returns the singleton instance
	Direct3D* const Direct3D::GetInstance(int a_iScreenWidth, int a_iScreenHeight, bool a_bVsync,
		HWND a_WindowHandle, bool a_bIsFullscreen, float a_fScreenDepth, float a_fScreenNear, PixelColor*& const a_pPixelColor, PixelColor*& const a_pPixelColorBackground)
	{
		if (s_pInstance == nullptr)
		{
			s_pInstance = new Direct3D(a_pPixelColor, a_pPixelColorBackground);
			if (!s_pInstance->IsInitializedD3D(a_iScreenWidth, a_iScreenHeight, a_bVsync,
				a_WindowHandle, a_bIsFullscreen, a_fScreenDepth, a_fScreenNear))
			{
				s_pInstance->Destroy();
				return nullptr;
			}
		}
		return s_pInstance;
	}

	//Destroys the singleton instance
	void Direct3D::Destroy()
	{
		if (s_pInstance != nullptr)
		{
			s_pInstance->m_pDxgiSwapChain->SetFullscreenState(FALSE, NULL);

			s_pInstance->m_pTex2D->Release();
			s_pInstance->m_pVertShader->Release();
			s_pInstance->m_pFragShader->Release();
			
			s_pInstance->m_pDxgiSwapChain->Release();
			s_pInstance->m_pRenderTargetView->Release();
			s_pInstance->m_pID3D11Device->Release();
			s_pInstance->m_pID3D11DevContext->Release();
			
			delete s_pInstance;
			s_pInstance = nullptr;
		}
	}


	//Called on start of the frame
	//Sets the entire frame color to the background color
	void Direct3D::BeginFrame()
	{
		//m_pID3D11DevContext->ClearRenderTargetView(m_pRenderTargetView, D3DXCOLOR(1.0f, 0.0f, 0.8f, 1.0f));
		memcpy_s(m_pPixelColor, m_iSizeOfPixelArray, m_pBackgroundColor, m_iSizeOfPixelArray);
	}

	//Called after the entire frame is drawn
	//Sets the buffers and swaps the back buffer with the front buffer
	void Direct3D::EndFrame()
	{
		//Maps the subresource 
		//On mapping, the CPU tells the GPU to halt any action being performed on the Tex2D until it is unmapped
		//Because the GPU might be accessing the Text2D in the background
		m_pID3D11DevContext->Map(m_pTex2D, 0, D3D11_MAP_WRITE_DISCARD, 0, &m_MappedBufferSubresource);  //Maps the subresource with the texture2D in memory

		PixelColor* l_pDestination = (PixelColor*)(m_MappedBufferSubresource.pData);

		const unsigned int l_uiBufferTextureWidth = m_MappedBufferSubresource.RowPitch / m_uiSizeOfSingleColor; // Total width of the Texture2D as m_MappedBufferSubresource.RowPitch = padding + pixelColor width
		const unsigned int l_uiBytesPerScreenRow = System::s_uiScreenWidth * m_uiSizeOfSingleColor;				//Width of the game window in bytes

		for (int y = 0; y < System::s_uiScreenHeight; y++)
		{
			memcpy_s(&l_pDestination[y * l_uiBufferTextureWidth], l_uiBytesPerScreenRow, &m_pPixelColor[y * System::s_uiScreenWidth], l_uiBytesPerScreenRow);
		}

		m_pID3D11DevContext->Unmap(m_pTex2D, 0);				//Give access of Tex2D back to the GPU	

		m_pID3D11DevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); //Sets how you wish to draw the vertex primitives
		const UINT l_uiStride = sizeof(VERTEX_DATA);				//Size of single vertex info in the vert buffer
		const UINT l_uiOffset = 0;									//The offset from the start of the vert buffer where in the first vert data is stored

		m_pID3D11DevContext->PSSetSamplers(0, 1, &m_pSamplerState);							//Sets sample on how you wish to draw the tex2D
		m_pID3D11DevContext->IASetVertexBuffers(0, 1, &m_pVertBuffer, &l_uiStride, &l_uiOffset);
		m_pID3D11DevContext->PSSetShaderResources(0, 1, &m_pShaderResourceView);			//Setting the resource to the frag shader

		m_pID3D11DevContext->Draw(6, 0);// Draws with the above information 
		m_pDxgiSwapChain->Present(1, 0);//Switches the front and back buffer
	}
}