#pragma once

#include <windows.h>
#include <windowsx.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include <d3dcompiler.h>

#include "PixelColor.h"
#include "../System/System.h"
#include <exception>

namespace FretBuzzSystem
{
	class Direct3D
	{
	private:
		static Direct3D* s_pInstance;

		Direct3D(PixelColor*& const a_PixelColor, PixelColor*& const a_PixelColorBackground );
		~Direct3D();


		bool IsInitializedD3D(int a_iScreenWidth, int a_iScreenHeight, bool a_bVsync,
			HWND a_WindowHandle, bool a_bIsFullscreen, float a_fScreenDepth, float a_fScreenNear);

		IDXGISwapChain* m_pDxgiSwapChain = nullptr;
		ID3D11Device* m_pID3D11Device = nullptr;
		ID3D11DeviceContext* m_pID3D11DevContext = nullptr;
		ID3D11RenderTargetView *m_pRenderTargetView = nullptr;

		//Vertex shader
		ID3D11VertexShader* m_pVertShader = nullptr;

		//Pixel shader
		ID3D11PixelShader* m_pFragShader = nullptr;

		//Vertex buffer
		ID3D11Buffer* m_pVertBuffer = nullptr;

		//Layout of the vertex buffer, that states how the GPU should read it
		ID3D11InputLayout* m_pInputLayout = nullptr;

		ID3D11ShaderResourceView* m_pShaderResourceView = nullptr;

		ID3D11SamplerState* m_pSamplerState = nullptr;

		D3D11_MAPPED_SUBRESOURCE m_MappedBufferSubresource;

		//The array of the pixels on the screen texture
		PixelColor* const m_pPixelColor = nullptr;

		//The array of the background color
		PixelColor* const m_pBackgroundColor = nullptr;

		unsigned int m_uiSizeOfSingleColor = 0;

		//Total number of pixels on the screen
		int m_iTotalPixels = 0;

		//Size of pixel array
		int m_iSizeOfPixelArray = 0;

		//Texture on which the frame will be rendered on
		ID3D11Texture2D* m_pTex2D = nullptr;

		//Determines the structur of vertex data we intend to send to the GPU
		struct VERTEX_DATA
		{
			FLOAT X, Y, Z;      
			FLOAT U, V;
		};

		void InitializeShaderPipeline();
		void InitializeVertexBufferToGPU();

		bool m_bIsCompileShaderRuntime = true;

	public:
		static Direct3D* const GetInstance(int a_iScreenWidth, int a_iScreenHeight, bool a_bVsync, 
			HWND a_WindowHandle, bool a_bIsFullscreen,float a_fScreenDepth, float a_fScreenNear,
			PixelColor*& const a_PixelColor, PixelColor*& const a_PixelColorBackground);

		static void Destroy();

		void BeginFrame();
		void EndFrame();
	};
}