#pragma once
#include "Timer.h"
#include "../Debugging/Debug.h"
#include <string>

namespace FretBuzzSystem
{
	//Singleton instance
	Timer* Timer::s_pInstance = nullptr;

	Timer::Timer()
	{
		m_CurrentTimePoint = std::chrono::steady_clock::now();
		m_LastTimePoint = std::chrono::steady_clock::now();

		m_pvectUpdateTimer = new std::vector<IFrameUpdateTimer*>();
		m_pvectLateUpdateTimer = new std::vector<ILateUpdateTimer*>();
	}

	Timer::~Timer()
	{

	}

	//Returns the singleton instance
	Timer* const Timer::GetInstance()
	{
		if (s_pInstance == nullptr)
		{
			s_pInstance = new Timer();
		}
		return s_pInstance;
	}

	//Destroys the timer instance
	void Timer::Destroy()
	{
		if (m_pvectUpdateTimer != nullptr)
		{
			delete m_pvectUpdateTimer;
			m_pvectUpdateTimer = nullptr;
		}

		if (m_pvectLateUpdateTimer != nullptr)
		{
			delete m_pvectLateUpdateTimer;
			m_pvectLateUpdateTimer = nullptr;
		}

		if (s_pInstance != nullptr)
		{
			delete s_pInstance;
			s_pInstance = nullptr;
		}
	}

#pragma region FrameUpdate

	//Calculates the delta time
	//Sends the delta time to all the objects that are registered for the OnUpdate event
	void Timer::FrameUpdate(const std::chrono::steady_clock::time_point& a_TimePoint)
	{
		m_LastTimePoint = m_CurrentTimePoint;
		m_CurrentTimePoint = a_TimePoint;
		std::chrono::duration<float> m_fDuration = m_CurrentTimePoint - m_LastTimePoint;
		m_fDeltaTime = m_fDuration.count();

		#ifdef IS_FRET_BUZZ_DEBUG
		m_uiCurrentFPS++;
		m_fTimePassedInSecond += m_fDeltaTime;

		if (m_fTimePassedInSecond >= ONE_SECOND)
		{
			m_fTimePassedInSecond -= ONE_SECOND;
			FretBuzzSystem::Debug::Log("Timer:: Current FPS :: "+ std::to_string(m_uiCurrentFPS));
			m_uiCurrentFPS = 0;
		}
		#endif

		for (auto l_itrCurrUpdateObj = m_pvectUpdateTimer->begin();
			l_itrCurrUpdateObj != m_pvectUpdateTimer->end(); l_itrCurrUpdateObj++)
		{
			IFrameUpdateTimer* const l_refCurrUpdateTimer = (*l_itrCurrUpdateObj);
			if (l_refCurrUpdateTimer->m_bIsUpdatable)
			{
				l_refCurrUpdateTimer->OnUpdate(m_fDeltaTime);
			}
		}
	}

	//Function is called to register the IUpdateTimer object for future Updates
	void Timer::AddFrameUpdateTimer(IFrameUpdateTimer* const &a_pUpdatableObject)
	{
		s_pInstance->m_pvectUpdateTimer->push_back(a_pUpdatableObject);
	}

	//Function is called to unregister the IUpdateTimer object for future Updates
	void Timer::RemoveFrameUpdateTimer(IFrameUpdateTimer* const &a_pUpdatableObject)
	{
		std::vector<IFrameUpdateTimer*>*& l_refUpdateVector = s_pInstance->m_pvectUpdateTimer;

		auto it = std::find(l_refUpdateVector->begin(), l_refUpdateVector->end(), a_pUpdatableObject);

		if (it != l_refUpdateVector->end())
		{
			std::swap(*it, l_refUpdateVector->back());
			l_refUpdateVector->pop_back();
		}
	}
#pragma endregion FrameUpdate

#pragma region LateUpdate

	//Sends the delta time to all the objects that are registered for the OnlateUpdate event
	//The LateUpdate is called at the end i.e. just before rendering the frame
	void Timer::FrameLateUpdate()
	{
		for (auto l_itrCurrLateUpdateObj = m_pvectLateUpdateTimer->begin();
			l_itrCurrLateUpdateObj != m_pvectLateUpdateTimer->end(); l_itrCurrLateUpdateObj++)
		{
			ILateUpdateTimer* const l_refCurrLateUpdateTimer = (*l_itrCurrLateUpdateObj);
			if (l_refCurrLateUpdateTimer->m_bIsUpdatable)
			{
				l_refCurrLateUpdateTimer->OnLateUpdate(m_fDeltaTime);
			}
		}
	}

	//Function is called to register the ILateUpdateTimer object for future Updates
	void Timer::AddLateUpdateTimer(ILateUpdateTimer* const &a_pUpdatableObject)
	{
		s_pInstance->m_pvectLateUpdateTimer->push_back(a_pUpdatableObject);
	}

	//Function is called to unregister the ILateUpdateTimer object for future Updates
	void Timer::RemoveLateUpdateTimer(ILateUpdateTimer* const &a_pLateUpdatableObject)
	{
		std::vector<ILateUpdateTimer*>*& l_refLateUpdateVector = s_pInstance->m_pvectLateUpdateTimer;

		auto it = std::find(l_refLateUpdateVector->begin(), l_refLateUpdateVector->end(), a_pLateUpdatableObject);

		if (it != l_refLateUpdateVector->end())
		{
			std::swap(*it, l_refLateUpdateVector->back());
			l_refLateUpdateVector->pop_back();
		}
	}

#pragma endregion  Lateupdate
}