#pragma once
#include <chrono>
#include <vector>

namespace FretBuzzSystem
{
#pragma region Timer
	class Timer
	{
	private:
		friend class Graphics;
		friend class ILateUpdateTimer;
		friend class IFrameUpdateTimer;

		static Timer* s_pInstance;

		Timer();
		~Timer();

		//the last calculated delta time
		float m_fDeltaTime = 0.0f;

		float m_fTimePassedInSecond = 0.0f;
		unsigned int m_uiCurrentFPS = 0;

		static constexpr float ONE_SECOND = 1.0f;

		static Timer* const GetInstance();
		void Destroy();

		std::chrono::steady_clock::time_point m_CurrentTimePoint;
		std::chrono::steady_clock::time_point m_LastTimePoint;

		void FrameUpdate(const std::chrono::steady_clock::time_point& a_TimePoint);
		void FrameLateUpdate();

		std::vector<IFrameUpdateTimer*>* m_pvectUpdateTimer = nullptr;
		std::vector<ILateUpdateTimer*>* m_pvectLateUpdateTimer = nullptr;
		
		static void AddFrameUpdateTimer(IFrameUpdateTimer* const &a_pUpdatableObject);
		static void RemoveFrameUpdateTimer(IFrameUpdateTimer* const &a_pUpdatableObject);

		static void AddLateUpdateTimer(ILateUpdateTimer* const &a_pUpdatableObject);
		static void RemoveLateUpdateTimer(ILateUpdateTimer* const &a_pUpdatableObject);
	};
#pragma endregion Timer

#pragma region IUpdate

	class IUpdateBase
	{
	public:
		//If true the update function for this object will be called else not
		bool m_bIsUpdatable = true;

		IUpdateBase(bool a_bIsUpdatable = true)
			:m_bIsUpdatable{ a_bIsUpdatable }
		{
		}

		virtual ~IUpdateBase() = 0
		{
		
		}
	};

#pragma endregion IUpdate

#pragma region IUpdateTimer

	class IFrameUpdateTimer : public virtual IUpdateBase
	{
	public:

		//The update function that will send the delta time
		virtual void OnUpdate(const float& a_fDeltaTime) = 0;

		//Constructor registers the class for update in the constructor
		IFrameUpdateTimer(bool a_bIsUpdatable = true)
			: IUpdateBase{a_bIsUpdatable}
		{
			Timer::AddFrameUpdateTimer(this);
		}

		virtual ~IFrameUpdateTimer() = 0
		{
			Timer::RemoveFrameUpdateTimer(this);
		};
	};

#pragma endregion IUpdateTimer


#pragma region ILateupdateTimer

	class ILateUpdateTimer : public virtual IUpdateBase
	{
	public:

		//The update function that will send the delta time
		virtual void OnLateUpdate(const float& a_fDeltaTime) = 0;

		//Constructor registers the class for update in the constructor
		ILateUpdateTimer(bool a_bIsUpdatable = true)
			: IUpdateBase{ a_bIsUpdatable }
		{
			Timer::AddLateUpdateTimer(this);
		}

		virtual ~ILateUpdateTimer() = 0
		{
			Timer::RemoveLateUpdateTimer(this);
		};
	};


#pragma endregion ILateupdateTimer

#pragma region IUpdateTimer

	class IUpdateTimer : public ILateUpdateTimer, public IFrameUpdateTimer
	{
	public:
		IUpdateTimer(const bool IsUpdatable = true)
			: ILateUpdateTimer{ IsUpdatable },
			IFrameUpdateTimer{IsUpdatable}
		{
		
		}

		virtual ~IUpdateTimer() = 0
		{
		
		}
	};

#pragma endregion IUpdateTimer
}