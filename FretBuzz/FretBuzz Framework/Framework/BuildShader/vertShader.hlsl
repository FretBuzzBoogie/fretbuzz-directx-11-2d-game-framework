struct VOut
{
	float4 m_Vert : SV_POSITION;
	float2 m_texCoord : PIXELCOORD0;
};

VOut vertMain( float4 position : VERT,float2 texCoord : PIXELCOORD0 )
{
	VOut output;

	output.m_Vert = position;
	output.m_texCoord = texCoord;

	return output;
}