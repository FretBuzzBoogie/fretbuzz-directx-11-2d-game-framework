Texture2D m_texture : register(t0);
SamplerState m_SamplerState : register(s0);

float4 fragMain(float4 position : SV_POSITION, float2 texCoord : TEXCOORD0) : SV_TARGET
{
	return m_texture.Sample(m_SamplerState,texCoord);
}