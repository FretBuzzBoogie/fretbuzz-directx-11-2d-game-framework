#pragma once
#include "System.h"
#include "../Graphics/Graphics.h"

namespace FretBuzzSystem
{
	//Singleton instance
	System* System:: s_pInstance = nullptr;

	unsigned int System::s_uiTopLeftX = 0;
	unsigned int System::s_uiTopLeftY = 0;
	unsigned int System::s_uiScreenWidth = 800;
	unsigned int System::s_uiScreenHeight = 600;

	//Constructor for the system
	System::System(LPSTR lpCmdLine, int nCmdShow) 
		: m_bIsFullScreen(false)
	{
	}

	//Destructor
	System::~System()
	{
	}

	//Creates, registers and shows the window
	bool System::IsInitializedWindow(LPSTR lpCmdLine, int nCmdShow)
	{
		s_pInstance->m_pDebug = Debug::GetInstance();
		if (s_pInstance->m_pDebug == nullptr)
		{
			return false;
		}
		ManageCMDLineArgs();

		WNDCLASSEX l_GameWindowClass;
		ZeroMemory(&l_GameWindowClass, sizeof(WNDCLASSEX));

		m_pProgramHandle = GetModuleHandle(NULL);

		l_GameWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		l_GameWindowClass.lpfnWndProc = WndProc_Game;
		l_GameWindowClass.cbClsExtra = 0;
		l_GameWindowClass.cbWndExtra = 0;
		l_GameWindowClass.hInstance = m_pProgramHandle;
		l_GameWindowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		l_GameWindowClass.hIconSm = l_GameWindowClass.hIcon;
		l_GameWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		//l_GameWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		l_GameWindowClass.lpszMenuName = NULL;
		l_GameWindowClass.lpszClassName = m_lpwstrClassName;
		l_GameWindowClass.cbSize = sizeof(WNDCLASSEX);

		if (!RegisterClassEx(&l_GameWindowClass))
		{
			MessageBox(NULL, L"Failed to register class", L"Failure", NULL);
			return false;
		}

		if(m_bIsFullScreen)
		{
			DEVMODE l_ScreenSettings;
			memset(&l_ScreenSettings, 0, sizeof(l_ScreenSettings));

			s_uiScreenWidth = GetSystemMetrics(SM_CXSCREEN);
			s_uiScreenHeight = GetSystemMetrics(SM_CYSCREEN);

			l_ScreenSettings.dmSize = sizeof(l_ScreenSettings);
			l_ScreenSettings.dmPelsWidth = (unsigned long)s_uiScreenWidth;
			l_ScreenSettings.dmPelsHeight = (unsigned long)s_uiScreenHeight;
			l_ScreenSettings.dmBitsPerPel = 32;
			l_ScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			ChangeDisplaySettings(&l_ScreenSettings, CDS_FULLSCREEN);

		}
		else
		{
			RECT l_rect;
			l_rect.left = (GetSystemMetrics(SM_CXSCREEN) - s_uiScreenWidth) / 2;
			l_rect.right = l_rect.left + s_uiScreenWidth;
			l_rect.top = (GetSystemMetrics(SM_CYSCREEN) - s_uiScreenHeight) / 2;
			l_rect.bottom = l_rect.top + s_uiScreenHeight;

			s_uiTopLeftX = l_rect.left;
			s_uiTopLeftY = l_rect.top;

			AdjustWindowRect(&l_rect, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, FALSE);
		}

		m_pWindowHandle = CreateWindowEx(WS_EX_APPWINDOW, m_lpwstrClassName, m_lpwstrWindowTitle,
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
			s_uiTopLeftX, s_uiTopLeftY, s_uiScreenWidth, s_uiScreenHeight, NULL, NULL, m_pProgramHandle, NULL);

		if (!m_pWindowHandle)
		{
			MessageBox(NULL, L"Failed to create window", L"Failure", NULL);
			return false;
		}

		ShowWindow(m_pWindowHandle, SW_SHOW);
		SetForegroundWindow(m_pWindowHandle);
		SetFocus(m_pWindowHandle);
		UpdateWindow(m_pWindowHandle);

		//ShowCursor(false);

		return true;
	}

	//Manage CMD arguements
	void System::ManageCMDLineArgs()
	{
		bool l_bIsWindowedSettingManaged = false;

		int l_iArgCount;
		LPWSTR* CmdArgsList = nullptr;
		CmdArgsList = CommandLineToArgvW(GetCommandLineW(), &l_iArgCount);

		if (CmdArgsList != NULL)
		{
			for (int l_iCmdIndex = 0; l_iCmdIndex < l_iArgCount; l_iCmdIndex++)
			{
				if (wcscmp(CMD_Debug, CmdArgsList[l_iCmdIndex]) == 0)
				{
					m_pDebug->CreateConsoleWindow();
					continue;
				}

				if (wcscmp(CMD_FULLSCREEN, CmdArgsList[l_iCmdIndex]) == 0 && !l_bIsWindowedSettingManaged)
				{
					m_bIsFullScreen = true;
					l_bIsWindowedSettingManaged = true;
				}
				else if (wcscmp(CMD_WINDOWED, CmdArgsList[l_iCmdIndex]) == 0 && !l_bIsWindowedSettingManaged)
				{
					m_bIsFullScreen = false;
					l_bIsWindowedSettingManaged = true;
				}
			}
			LocalFree(CmdArgsList);
		}
	}

	//The event handler to be called by the window
	LRESULT WINAPI System::WndProc_Game(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch (message)
		{
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_CLOSE:
		{
			PostQuitMessage(0);
			break;
		}

		default:
			s_pInstance->MessageHandler(hWnd, message, wParam, lParam);
			break;
		}

		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	//Handles the message of the system 
	LRESULT WINAPI System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
	{
	// Check if a key has been released on the keyboard.
		switch (umsg)
		{
		#pragma region Keyboard Input
			case WM_KEYDOWN:
			{
				m_pInput->OnKeyDown((unsigned char)wparam);
				break;
			}
			case WM_KEYUP:
			{
				m_pInput->OnKeyUp((unsigned char)wparam);
				break;
			}
		#pragma endregion Keyboard Input

		#pragma region Mouse input

			case WM_MOUSEMOVE:
			{
				int l_iMouseX = GET_X_LPARAM(lparam);
				int l_iMouseY = GET_Y_LPARAM(lparam);

				//TODO:: Fix and test mouse move onenter and onexit

				if (l_iMouseX > 0 &&
					l_iMouseX < s_uiScreenWidth &&
					l_iMouseY > 0 &&
					l_iMouseY < s_uiScreenHeight)
				{
					if (!m_pInput->m_bIsMouseInWindow)
					{
						SetCapture(m_pWindowHandle);
						m_pInput->OnMouseEnterWindow();
					}
					m_pInput->m_bIsMouseInWindow = true;
					m_pInput->OnMouseMove(l_iMouseX, l_iMouseY);
				}
				else
				{
					if (m_pInput->m_bIsMouseInWindow)
					{
						m_pInput->OnMouseExitWindow();
					}
					m_pInput->m_bIsMouseInWindow = false;

					if (wparam & (MK_LBUTTON | MK_RBUTTON))
					{
						l_iMouseX = min(s_uiScreenWidth, l_iMouseX);
						l_iMouseX = max(0, l_iMouseX);
						l_iMouseY = min(s_uiScreenHeight, l_iMouseY);
						l_iMouseY = max(0, l_iMouseY);

						m_pInput->OnMouseMove(l_iMouseX, l_iMouseY);
					}
					else
					{
						ReleaseCapture();
					}
				}
				break;
			}

			case WM_LBUTTONDOWN:
				m_pInput->OnKeyDown((unsigned char)VK_LBUTTON);
				break;

			case WM_LBUTTONUP:
				m_pInput->OnKeyUp((unsigned char)VK_LBUTTON);
				break;

			case WM_RBUTTONDOWN:
				m_pInput->OnKeyDown((unsigned char)VK_RBUTTON);
				break;

			case WM_RBUTTONUP:
				m_pInput->OnKeyUp((unsigned char)VK_RBUTTON);
				break;

			case WM_MBUTTONDOWN:
				m_pInput->OnKeyDown((unsigned char)VK_MBUTTON);
				break;

			case WM_MBUTTONUP:
				m_pInput->OnKeyUp((unsigned char)VK_MBUTTON);
				break;

			case WM_MOUSEWHEEL:
			{
				int l_iMouseScroll = GET_WHEEL_DELTA_WPARAM(wparam);
				if (l_iMouseScroll > 0)
				{
					m_pInput->OnMouseWheelScrollForward(l_iMouseScroll);
				}
				else if(l_iMouseScroll < 0)
				{
					m_pInput->OnMouseWheelScrollBackward(l_iMouseScroll);
				}

				break;
			}

		#pragma endregion Mouse input

			default:
			{
				return DefWindowProc(hwnd, umsg, wparam, lparam);
			}
		}
	}

	//Checks if the system has been properly initialized
	bool System::IsInitializedSystem(LPSTR lpCmdLine, int nCmdShow)
	{
		if (!s_pInstance->IsInitializedWindow(lpCmdLine, nCmdShow))
			{
				return false;
			}

			s_pInstance->m_pInput = Input::GetInstance();
			if (s_pInstance->m_pInput == nullptr)
			{
				return false;
			}

			s_pInstance->m_pGraphics = Graphics::GetInstance(m_bIsFullScreen);
			if (s_pInstance->m_pGraphics == nullptr)
			{
				return false;
			}

			return true;
	}

	//Runs the functionality to display a frame on the screen
	void System::RunFrame()
	{
		m_pGraphics->Render();
	}

	//Gets the singleton instance
	//Creates an instance if not already created
	System* const System::GetInstance(LPSTR lpCmdLine, int nCmdShow)
	{
		if (s_pInstance == nullptr)
		{
			s_pInstance = new System(lpCmdLine, nCmdShow);

			if (!s_pInstance->IsInitializedSystem(lpCmdLine, nCmdShow))
			{
				s_pInstance->Destroy();
				return nullptr;
			}
		}
		return s_pInstance;
	}

	//Checks if nullptr, Destroys the singleton instanc
	void System::Destroy()
	{
		Input::Destroy();
		Graphics::Destroy();
		Debug::Destroy();

		if (s_pInstance != nullptr)
		{
			delete s_pInstance;
			s_pInstance = nullptr;
		}
	}

	//Runs the game loop
	void System::Run()
	{
		MSG l_message;
		ZeroMemory(&l_message, sizeof(MSG));

		while (true)
		{
			if (PeekMessage(&l_message, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&l_message);
				DispatchMessage(&l_message);

				if (l_message.message == WM_QUIT)
				{
					break;
				}
			}
			else
			{
				RunFrame();
			}
		}
	}

	//returns the x-co-ordinate of the top left of the window
	int System::GetTopLeftX()
	{
		return s_pInstance->s_uiTopLeftX;
	}

	//returns the y-co-ordinate of the top left of the window
	int System::GetTopLeftY()
	{
		return s_pInstance->s_uiTopLeftY;
	}

	//Returns the screen width
	int System::GetScreenWidth()
	{
		return s_pInstance->s_uiScreenWidth;
	}

	//Returns the screen height
	int System::GetScreenHeight()
	{
		return s_pInstance->s_uiScreenHeight;
	}

	//returns the window handle of the game
	HWND const System::GetWindowhandle()
	{
		return s_pInstance->m_pWindowHandle;
	}
}