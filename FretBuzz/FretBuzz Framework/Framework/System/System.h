#pragma once
#include <windows.h>
#include "..\Input\Input.h"
#include "..\Debugging\Debug.h"

namespace FretBuzzSystem
{
	class Graphics;

	class System
	{
	private:
		static System* s_pInstance;

		friend class Graphics;
		friend class Direct3D;

		static unsigned int s_uiTopLeftX;
		static unsigned int s_uiTopLeftY;
		static unsigned int s_uiScreenWidth;
		static unsigned int s_uiScreenHeight;

		Input* m_pInput = nullptr;
		Graphics* m_pGraphics = nullptr;
		Debug* m_pDebug = nullptr;

		System(LPSTR lpCmdLine, int nCmdShow);
		~System();

		LPWSTR m_lpwstrWindowTitle = L"Fret Buzz";
		LPWSTR m_lpwstrClassName = L"FretBuzzClassName";

		HINSTANCE m_pProgramHandle = nullptr;
		HWND m_pWindowHandle = nullptr;

		bool IsInitializedWindow(LPSTR lpCmdLine, int nCmdShow);
		bool IsInitializedSystem(LPSTR lpCmdLine, int nCmdShow);

		static LRESULT WINAPI WndProc_Game(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		LRESULT WINAPI MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);

		//Cmd line arguement that tells to run the debug program
		LPWSTR CMD_Debug = L"debug";

		//Cmd line arguement that says to run the fullscreen program
		LPWSTR CMD_FULLSCREEN = L"fullscreen";

		//Cmd line arguement that says to run the windowed program
		LPWSTR CMD_WINDOWED = L"windowed";

		bool m_bIsFullScreen;

		void ManageCMDLineArgs();

		void RunFrame();

	public:

		static System* const GetInstance(LPSTR lpCmdLine, int nCmdShow);
		static void Destroy();

		void Run();


		static int GetTopLeftX();
		static int GetTopLeftY();
		static int GetScreenWidth();
		static int GetScreenHeight();
		static HWND const GetWindowhandle();
	};

}

