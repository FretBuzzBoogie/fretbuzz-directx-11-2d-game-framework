#pragma once
#include "Vertex2D.h"

namespace FretBuzzMath
{
	Vertex2D::Vertex2D()
	{
		
	}

	Vertex2D::Vertex2D(Vec2 a_v2Texture, Vec2 a_v2Model)
		: m_v2Texture{ a_v2Texture },
		  m_v2Model{a_v2Model}
	{
	}

	void Vertex2D:: Swap(Vertex2D& a_vert2D)
	{
		Vertex2D l_vert2D = *this;
		*this = a_vert2D;
		a_vert2D = l_vert2D;
	}

	void Vertex2D::operator=(Vertex2D a_vert2D)
	{
		this->m_v2Model = a_vert2D.m_v2Model;
		this->m_v2Texture = a_vert2D.m_v2Texture;
	}
}