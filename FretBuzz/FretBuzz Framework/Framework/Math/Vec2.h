#pragma once

namespace FretBuzzMath
{
	struct Vec2
	{
		float X = 0.0f;
		float Y = 0.0f;

		Vec2() = default;
		Vec2(const float a_X, const float a_Y);
		~Vec2();

		Vec2& Add(const Vec2& a_Vec2);
		Vec2& Add(const float a_Scalar);
		Vec2 operator+(const Vec2& a_Vec2) const;
		Vec2 operator+(const float a_Scalar) const;
		Vec2& operator+=(const Vec2& a_Vec2);
		Vec2& operator+=(const float a_Scalar);

		Vec2& Subtract(const Vec2& a_Vec2);
		Vec2& Subtract(const float a_Scalar);
		Vec2 operator-(const Vec2& a_Vec2) const;
		Vec2 operator-(const float a_Scalar) const;
		Vec2& operator-=(const Vec2& a_Vec2);
		Vec2& operator-=(const float a_Scalar);

		Vec2& Divide(const Vec2& a_Vec2);
		Vec2& Divide(const float a_Scalar);
		Vec2 operator/(const Vec2& a_Vec2) const;
		Vec2 operator/(const float a_Scalar) const;
		Vec2& operator/=(const Vec2& a_Vec2);
		Vec2& operator/=(const float a_Scalar);

		Vec2& Multiply(const Vec2& a_Vec2);
		Vec2& Multiply(const float a_Scalar);
		Vec2 operator*(const Vec2& a_Vec2) const;
		Vec2 operator*(const float a_Scalar) const;
		Vec2& operator*=(const Vec2& a_Vec2);
		Vec2& operator*=(const float a_Scalar);

		void operator=(const Vec2& a_Vec2);
		Vec2 operator-() const;

		float GetMagnitude() const;
		Vec2 GetNormalize();
		float Dot(const Vec2& a_Vec2);

		void Rotate(const float& a_fRotAngle);
	};
}