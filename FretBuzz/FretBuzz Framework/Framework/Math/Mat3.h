#pragma once
#include "Vec2.h"

namespace FretBuzzMath
{
	struct Mat3
	{
	private:
		static Mat3 const IDENTITY;

	public:
		float m_arrMat[3][3];

		static Mat3 GetIdentity();
			
		Mat3& Multiply(const Mat3& a_Mat3);
		Mat3 operator*(const Mat3& a_Mat3) const;
		void operator*=(const Mat3& a_Mat3);

		Mat3& Add(const Mat3& a_Mat3);
		Mat3 operator+(const Mat3& a_Mat3);
		void operator+=(const Mat3& a_Mat3);

		Vec2 operator*(const Vec2& a_Vec2);

		static Mat3 Rotate(const float& a_fAngle);
		static Mat3 Scale(const float& Scalar);
		static Mat3 Translate(const FretBuzzMath::Vec2& a_vec2);

		void operator=(const Mat3& a_Mat3);
	};
}