#pragma once
#include "Vec2.h"

namespace FretBuzzMath
{
	struct Vertex2D
	{
	public:

		Vec2 m_v2Texture;
		Vec2 m_v2Model;

		Vertex2D();
		Vertex2D(Vec2 a_v2Texture, Vec2 a_v2Model);

		virtual void Swap(Vertex2D& a_vert2D);
		void operator=(Vertex2D a_vert2D);
	};
}