#pragma once
#include "Rect.h"
#include <cmath>

namespace FretBuzzMath
{
	Rect::Rect(float a_fTop, float a_fBottom, float a_fLeft, float a_fRight)
	{
		SetRect(a_fTop, a_fBottom, a_fLeft, a_fRight);
	}

	Rect::Rect(const Rect& a_Rect)
	{
		SetRect(a_Rect);
	}

	Rect::~Rect()
	{
		
	}

	//Returns the width of the rect
	unsigned int Rect::GetWidth()
	{
		m_fWidth = m_fRight - m_fLeft;
		return m_fWidth;
	}

	//Returns the height of the rect
	unsigned int Rect::GetHeight()
	{
		m_fHeight = m_fBottom - m_fTop;
		return m_fHeight;
	}

	//Sets the rect edge position
	void Rect::SetRect(const FretBuzzMath::Vec2& a_TopLeft)
	{
		m_fTop = a_TopLeft.Y;
		m_fBottom = m_fTop + m_fHeight;
		m_fLeft = a_TopLeft.X;
		m_fRight = m_fLeft + m_fWidth;
	}

	//Sets the rect edge position
	void Rect::SetRect(float a_fTop, float a_fBottom, float a_fLeft, float a_fRight)
	{
		m_fTop = a_fTop;
		m_fLeft = a_fLeft;
		m_fBottom = a_fBottom < a_fTop ? a_fTop : a_fBottom;
		m_fRight = a_fRight < a_fLeft ? a_fLeft : a_fRight;

		m_fWidth = m_fRight - m_fLeft;
		m_fHeight = m_fBottom - m_fTop;
	}

	//Sets the rect edge position
	void Rect::SetRect(const Rect& a_Rect)
	{
		SetRect(a_Rect.m_fTop, a_Rect.m_fBottom, a_Rect.m_fLeft, a_Rect.m_fRight);
	}

	//returns a Rect that will clip this with the arguement Rect
	void Rect::ClipWithRect(const Rect& a_Rect)
	{
		/*float l_fLeft = std::fmax(a_Rect.m_fLeft, m_fLeft);
		float l_fRight = std::fmin(a_Rect.m_fRight, m_fRight);
		float l_fTop = std::fmax(a_Rect.m_fTop, m_fTop);
		float l_fBottom = std::fmin(a_Rect.m_fBottom, m_fBottom);*/

		return SetRect(std::fmax(a_Rect.m_fTop, m_fTop),
			std::fmin(a_Rect.m_fBottom, m_fBottom),
			std::fmax(a_Rect.m_fLeft, m_fLeft),
			std::fmin(a_Rect.m_fRight, m_fRight));
	}
}