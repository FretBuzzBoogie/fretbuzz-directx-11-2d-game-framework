#pragma once

namespace FretBuzzMath
{
	struct Vec3
	{
		float X = 0.0f;
		float Y = 0.0f;
		float Z = 0.0f;

		Vec3() = default;
		Vec3(const float a_X, const float a_Y, const float a_Z);
		~Vec3();

		Vec3& Add(const Vec3& a_Vec3);
		Vec3& Add(const float a_Scalar);
		Vec3 operator+(const Vec3& a_Vec3) const;
		Vec3 operator+(const float a_Scalar) const;
		Vec3& operator+=(const Vec3& a_Vec3);
		Vec3& operator+=(const float a_Scalar);

		Vec3& Subtract(const Vec3& a_Vec3);
		Vec3& Subtract(const float a_Scalar);
		Vec3 operator-(const Vec3& a_Vec3) const;
		Vec3 operator-(const float a_Scalar) const;
		Vec3& operator-=(const Vec3& a_Vec3);
		Vec3& operator-=(const float a_Scalar);

		Vec3& Divide(const Vec3& a_Vec3);
		Vec3& Divide(const float a_Scalar);
		Vec3 operator/(const Vec3& a_Vec3) const;
		Vec3 operator/(const float a_Scalar) const;
		Vec3& operator/=(const Vec3& a_Vec3);
		Vec3& operator/=(const float a_Scalar);

		Vec3& Multiply(const Vec3& a_Vec3);
		Vec3& Multiply(const float a_Scalar);
		Vec3 operator*(const Vec3& a_Vec3) const;
		Vec3 operator*(const float a_Scalar) const;
		Vec3& operator*=(const Vec3& a_Vec3);
		Vec3& operator*=(const float a_Scalar);

		void operator=(const Vec3& a_Vec3);

		float GetMagnitude() const;
		Vec3 GetNormalize();
		float Dot(const Vec3& a_Vec3);
	};
}