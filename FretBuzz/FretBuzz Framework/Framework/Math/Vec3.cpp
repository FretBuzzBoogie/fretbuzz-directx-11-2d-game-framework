#pragma once
#include "Vec3.h"
#include <cmath>

namespace FretBuzzMath
{
	Vec3::Vec3(const float a_X, const float a_Y, const float a_Z)
		: X{ a_X }, Y{ a_Y }, Z{a_Z}
	{
	}

	Vec3::~Vec3()
	{
	}

	////////////////////ADDITION//////////////////////////

	//Adds the input vector with this
	Vec3& Vec3::Add(const Vec3& a_Vec3)
	{
		return *this += a_Vec3;
	}

	//Adds the float vector with this
	Vec3& Vec3::Add(const float a_Scalar)
	{
		return *this += a_Scalar;
	}

	//Adds this with the input vector and returns the this
	Vec3& Vec3::operator+=(const Vec3& a_Vec3)
	{
		X += a_Vec3.X;
		Y += a_Vec3.Y;
		Z += a_Vec3.Z;
		return *this;
	}

	//Adds this with the input scalar and returns the this
	Vec3& Vec3::operator+=(const float a_Scalar)
	{
		X += a_Scalar;
		Y += a_Scalar;
		Z += a_Scalar;
		return *this;
	}

	//Adds this with the input Vector and returns the result
	Vec3 Vec3::operator+(const Vec3& a_Vec3) const
	{
		return Vec3(X + a_Vec3.X, Y + a_Vec3.Y, Z + a_Vec3.Z);
	}

	//Adds this with the input scalar and returns the result
	Vec3 Vec3::operator+(const float a_Scalar) const
	{
		return Vec3(X + a_Scalar, Y + a_Scalar, Z + a_Scalar);
	}


	////////////////////SUBTRACTION//////////////////////////

	//Subtracts this with the input scalar and returns the result
	Vec3& Vec3::Subtract(const Vec3& a_Vec3)
	{
		return *this -= a_Vec3;
	}

	//Subtracts the float vector with this
	Vec3& Vec3::Subtract(const float a_Scalar)
	{
		return *this -= a_Scalar;
	}

	//Subtracts this with the input vector and returns the this
	Vec3& Vec3::operator-=(const Vec3& a_Vec3)
	{
		X -= a_Vec3.X;
		Y -= a_Vec3.Y;
		Z -= a_Vec3.Z;
		return *this;
	}

	//Subtracts this with the input scalar and returns the this
	Vec3& Vec3::operator-=(const float a_Scalar)
	{
		X -= a_Scalar;
		Y -= a_Scalar;
		Z -= a_Scalar;
		return *this;
	}

	//Subtracts this with the input Vector and returns the result
	Vec3 Vec3::operator-(const Vec3& a_Vec3) const
	{
		return Vec3(X - a_Vec3.X, Y - a_Vec3.Y, Z - a_Vec3.Z);
	}

	//Subtracts this with the input scalar and returns the result
	Vec3 Vec3::operator-(const float a_Scalar) const
	{
		return Vec3(X - a_Scalar, Y - a_Scalar, Z - a_Scalar);
	}


	////////////////////DIVISION//////////////////////////

	//Divides this with the input vector and returns the this
	Vec3& Vec3::Divide(const Vec3& a_Vec3)
	{
		return *this /= a_Vec3;
	}

	//Divides the float vector with this
	Vec3& Vec3::Divide(const float a_Scalar)
	{
		return *this /= a_Scalar;
	}

	//Divides this with the input vector and returns the this
	Vec3 Vec3::operator/(const Vec3& a_Vec3) const
	{
		return Vec3(X / a_Vec3.X, Y / a_Vec3.Y, Z / a_Vec3.Z);
	}

	//Divides this with the input scalar and returns the result
	Vec3 Vec3::operator/(const float a_Scalar) const
	{
		return Vec3(X / a_Scalar, Y / a_Scalar, Z / a_Scalar);
	}

	//Subtracts this with the input vector and returns the this
	Vec3& Vec3::operator/=(const Vec3& a_Vec3)
	{
		X /= a_Vec3.X;
		Y /= a_Vec3.Y;
		Z /= a_Vec3.Z;
		return *this;
	}

	//Subtracts this with the input scalar and returns the this
	Vec3& Vec3::operator/=(const float a_Scalar)
	{
		X /= a_Scalar;
		Y /= a_Scalar;
		Z /= a_Scalar;
		return *this;
	}


	////////////////////MULTIPLICATION//////////////////////////

	//Multiplies this with the input vector and returns the this
	Vec3& Vec3::Multiply(const Vec3& a_Vec3)
	{
		return *this *= a_Vec3;
	}

	//Multiplies the float vector with this
	Vec3& Vec3::Multiply(const float a_Scalar)
	{
		return *this *= a_Scalar;
	}

	//Multiplies this with the input vector and returns the this
	Vec3 Vec3::operator*(const Vec3& a_Vec3) const
	{
		return Vec3(X * a_Vec3.X, Y * a_Vec3.Y, Z * a_Vec3.Z);
	}

	//Multiplies Divides this with the input scalar and returns the result
	Vec3 Vec3::operator*(const float a_Scalar) const
	{
		return Vec3(X * a_Scalar, Y * a_Scalar, Z * a_Scalar);
	}

	//Multiplies this with the input vector and returns the this
	Vec3& Vec3::operator*=(const Vec3& a_Vec3)
	{
		X *= a_Vec3.X;
		Y *= a_Vec3.Y;
		Z *= a_Vec3.Z;
		return *this;
	}

	//Multiplies this with the input scalar and returns the this
	Vec3& Vec3::operator*=(const float a_Scalar)
	{
		X *= a_Scalar;
		Y *= a_Scalar;
		Z *= a_Scalar;
		return *this;
	}

	//Sets the values of this  to a_Vec2
	void Vec3::operator=(const Vec3& a_Vec3)
	{
		X = a_Vec3.X;
		Y = a_Vec3.Z;
		Z = a_Vec3.Z;
	}

	//Returns magnitude of the vector
	float Vec3::GetMagnitude() const
	{
		return sqrtf(X * X + Y * Y + Z * Z);
	}

	//Returns the normalized vector
	Vec3 Vec3::GetNormalize()
	{
		float l_fMagnitude = GetMagnitude();
		return Vec3(X / l_fMagnitude, Y / l_fMagnitude, Z / l_fMagnitude);
	}

	//Returns Dot product
	float Vec3::Dot(const Vec3& a_Vec3)
	{
		float l_fOtherMagnitude = a_Vec3.GetMagnitude();
		float l_fThisMagnitude = GetMagnitude();

		return (a_Vec3.X / l_fOtherMagnitude * X / l_fThisMagnitude,
			a_Vec3.Y / l_fOtherMagnitude * Y / l_fThisMagnitude,
			a_Vec3.Z / l_fOtherMagnitude * Z / l_fThisMagnitude);
	}
}