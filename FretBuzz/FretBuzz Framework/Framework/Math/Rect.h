#pragma once
#include "Vec2.h"

namespace FretBuzzMath
{
	struct Rect
	{
	protected:
		float m_fWidth  = 0.0f;
		float m_fHeight = 0.0f;

	public:
		float m_fTop	= 0.0f;
		float m_fBottom = 0.0f;
		float m_fLeft	= 0.0f;
		float m_fRight	= 0.0f;

		Rect() = delete;
		Rect(const Rect& a_Rect);
		Rect(float a_fTop, float a_fBottom, float a_fLeft, float a_fRight);
		~Rect();

		unsigned int GetWidth();
		unsigned int GetHeight();

		void SetRect(const FretBuzzMath::Vec2& a_TopLeft);
		void SetRect(float a_fTop, float a_fBottom, float a_fLeft, float a_fRight);
		void SetRect(const Rect& a_Rect);

		void ClipWithRect(const Rect& a_Rect);
	};
}