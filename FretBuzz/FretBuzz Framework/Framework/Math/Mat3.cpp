#pragma once
#include "Mat3.h"
#include <string.h>
#include <cmath>
#include "Vec3.h"

namespace FretBuzzMath
{
	 Mat3 const Mat3:: IDENTITY = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };

	////Multiplies this with the arguement matrix
	Mat3& Mat3::Multiply(const Mat3& a_Mat3)
	{
		*this = *this * a_Mat3;
		return *this;
	}

	//Returns a Mat3 which is the multiplication of this an the arguement Mat3
	Mat3 Mat3::operator*(const Mat3& a_Mat3) const
	{
		Mat3 l_Mat3Result = IDENTITY;
		for (int i = 0; i < 3; i++)
		{
			for (int k = 0; k < 3; k++)
			{
				float l_iIndexValue = 0;
				for (int j = 0; j < 3; j++)
				{
					l_iIndexValue += m_arrMat[i][j] * a_Mat3.m_arrMat[j][k];
				}
				l_Mat3Result.m_arrMat[i][k] = l_iIndexValue;
			}
		}
		return l_Mat3Result;
	}

	//Multiplies this with the arguement matrix
	void Mat3::operator*=(const Mat3& a_Mat3)
	{
		this->Multiply(a_Mat3);
	}

	//Arrs this->Mat3 with the arguement Mat3 and returns this as the result
	Mat3& Mat3::Add(const Mat3& a_Mat3)
	{
		*this = *this + a_Mat3;
		return *this;
	}

	//Adds this->Mat3 with the arguement Mat3 and return the result
	Mat3 Mat3::operator+(const Mat3& a_Mat3)
	{
		Mat3 l_Mat3Return = IDENTITY;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				l_Mat3Return.m_arrMat[i][j] = m_arrMat[i][j] + a_Mat3.m_arrMat[i][j];
			}
		}
		return l_Mat3Return;
	}

	//Arrs this->Mat3 with the arguement Mat3
	void Mat3::operator+=(const Mat3& a_Mat3)
	{
		*this += a_Mat3;
	}

	//Returns a vec2 which is a result of multiplication of the input vector with the this->Matrix
	Vec2 Mat3::operator*(const Vec2& a_Vec2)
	{
		Vec2 l_Vec2Result;
		l_Vec2Result.X = m_arrMat[0][0] * a_Vec2.X + m_arrMat[0][1] * a_Vec2.Y + m_arrMat[0][2] * 1.0f;
		l_Vec2Result.Y = m_arrMat[1][0] * a_Vec2.X + m_arrMat[1][1] * a_Vec2.Y + m_arrMat[1][2] * 1.0f;
		return l_Vec2Result;
	}

	//Returns a matrix for rotation by a_fAngle
	Mat3 Mat3::Rotate(const float& a_fAngle)
	{
		float l_fSineTheta = sinf(a_fAngle);
		float l_fCosTheta = cosf(a_fAngle);
		return { l_fCosTheta, l_fSineTheta, 0.0f, -l_fSineTheta, l_fCosTheta,0.0f, 0.0f, 0.0f ,1.0f };
	}

	//Returns a matrix for scaling by the givenn factor 
	Mat3 Mat3::Scale(const float& a_fScalar)
	{
		return { a_fScalar, 0, 0, 0, a_fScalar, 0, 0 , 0, 1};
	}

	//Returns a matrix for scaling by the givenn Vec2
	Mat3 Mat3::Translate(const FretBuzzMath::Vec2& a_vec2)
	{
		return { 1.0f, 0.0f, a_vec2.X , 0.0f, 1.0f, a_vec2.Y, 0.0f , 0.0f, 1.0f };
	}

	void Mat3::operator=(const Mat3& a_Mat3)
	{
		memcpy(this, &a_Mat3,sizeof(Mat3));
	}

	//Returns the identity matrix
	Mat3 Mat3::GetIdentity()
	{
		return IDENTITY;
	}
}