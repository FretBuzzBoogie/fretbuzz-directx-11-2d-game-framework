#pragma once
#include "Vec2.h"
#include <cmath>

namespace FretBuzzMath
{
	Vec2::Vec2(const float a_X, const float a_Y)
		: X{ a_X }, Y{ a_Y }
	{
	}

	Vec2::~Vec2()
	{
	}

	////////////////////ADDITION//////////////////////////

	//Adds the input vector with this
	Vec2& Vec2::Add(const Vec2& a_Vec2)
	{
		return *this += a_Vec2;
	}

	//Adds the float vector with this
	Vec2& Vec2::Add(const float a_Scalar)
	{
		return *this += a_Scalar;
	}

	//Adds this with the input vector and returns the this
	Vec2& Vec2::operator+=(const Vec2& a_Vec2)
	{
		X += a_Vec2.X;
		Y += a_Vec2.Y;
		return *this;
	}

	//Adds this with the input scalar and returns the this
	Vec2& Vec2::operator+=(const float a_Scalar)
	{
		X += a_Scalar;
		Y += a_Scalar;
		return *this;
	}

	//Adds this with the input Vector and returns the result
	Vec2 Vec2::operator+(const Vec2& a_Vec2) const
	{
		return Vec2(X + a_Vec2.X, Y + a_Vec2.Y);
	}

	//Adds this with the input scalar and returns the result
	Vec2 Vec2::operator+(const float a_Scalar) const
	{
		return Vec2(X + a_Scalar, Y + a_Scalar);
	}


	////////////////////SUBTRACTION//////////////////////////

	//Subtracts this with the input scalar and returns the result
	Vec2& Vec2::Subtract(const Vec2& a_Vec2)
	{
		return *this -= a_Vec2;
	}

	//Subtracts the float vector with this
	Vec2& Vec2::Subtract(const float a_Scalar)
	{
		return *this -= a_Scalar;
	}

	//Subtracts this with the input vector and returns the this
	Vec2& Vec2::operator-=(const Vec2& a_Vec2)
	{
		X -= a_Vec2.X;
		Y -= a_Vec2.Y;
		return *this;
	}

	//Subtracts this with the input scalar and returns the this
	Vec2& Vec2::operator-=(const float a_Scalar)
	{
		X -= a_Scalar;
		Y -= a_Scalar;
		return *this;
	}

	//Subtracts this with the input Vector and returns the result
	Vec2 Vec2::operator-(const Vec2& a_Vec2) const
	{
		return Vec2(X - a_Vec2.X, Y - a_Vec2.Y);
	}

	//Subtracts this with the input scalar and returns the result
	Vec2 Vec2::operator-(const float a_Scalar) const
	{
		return Vec2(X - a_Scalar, Y - a_Scalar);
	}
	

	////////////////////DIVISION//////////////////////////

	//Divides this with the input vector and returns the this
	Vec2& Vec2::Divide(const Vec2& a_Vec2)
	{
		return *this /= a_Vec2;
	}

	//Divides the float vector with this
	Vec2& Vec2::Divide(const float a_Scalar)
	{
		return *this /= a_Scalar;
	}

	//Divides this with the input vector and returns the this
	Vec2 Vec2::operator/(const Vec2& a_Vec2) const
	{
		return Vec2(X / a_Vec2.X, Y / a_Vec2.Y);
	}

	//Divides this with the input scalar and returns the result
	Vec2 Vec2::operator/(const float a_Scalar) const
	{
		return Vec2(X / a_Scalar, Y / a_Scalar);
	}

	//Subtracts this with the input vector and returns the this
	Vec2& Vec2::operator/=(const Vec2& a_Vec2)
	{
		X /= a_Vec2.X;
		Y /= a_Vec2.Y;
		return *this;
	}

	//Subtracts this with the input scalar and returns the this
	Vec2& Vec2::operator/=(const float a_Scalar)
	{
		X /= a_Scalar;
		Y /= a_Scalar;
		return *this;
	}


	////////////////////MULTIPLICATION//////////////////////////

	//Multiplies this with the input vector and returns the this
	Vec2& Vec2::Multiply(const Vec2& a_Vec2)
	{
		return *this *= a_Vec2;
	}

	//Multiplies the float vector with this
	Vec2& Vec2::Multiply(const float a_Scalar)
	{
		return *this *= a_Scalar;
	}

	//Multiplies this with the input vector and returns the this
	Vec2 Vec2::operator*(const Vec2& a_Vec2) const
	{
		return Vec2(X * a_Vec2.X, Y * a_Vec2.Y);
	}

	//Multiplies Divides this with the input scalar and returns the result
	Vec2 Vec2::operator*(const float a_Scalar) const
	{
		return Vec2(X * a_Scalar, Y * a_Scalar);
	}

	//Multiplies this with the input vector and returns the this
	Vec2& Vec2::operator*=(const Vec2& a_Vec2)
	{
		X *= a_Vec2.X;
		Y *= a_Vec2.Y;
		return *this;
	}

	//Multiplies this with the input scalar and returns the this
	Vec2& Vec2::operator*=(const float a_Scalar)
	{
		X *= a_Scalar;
		Y *= a_Scalar;
		return *this;
	}

	//Sets the values of this  to a_Vec2
	void Vec2 :: operator=(const Vec2& a_Vec2)
	{
		X = a_Vec2.X;
		Y = a_Vec2.Y;
	}

	//Returns a negated vec2
	Vec2 Vec2::operator-() const
	{
		return { -this->X, -this->Y };
	}

	//Returns magnitude of the vector
	float Vec2::GetMagnitude() const
	{
		return sqrtf(X * X + Y * Y);
	}

	//Returns the normalized vector
	Vec2 Vec2::GetNormalize()
	{
		float l_fMagnitude = GetMagnitude();
		return Vec2(X / l_fMagnitude, Y / l_fMagnitude);
	}

	//Returns Dot product
	float Vec2::Dot(const Vec2& a_Vec2)
	{
		float l_fOtherMagnitude = a_Vec2.GetMagnitude();
		float l_fThisMagnitude = GetMagnitude();

		return (a_Vec2.X / l_fOtherMagnitude * X / l_fThisMagnitude,
			a_Vec2.Y / l_fOtherMagnitude * Y / l_fThisMagnitude);
	}

	//Rotates the vec2 by the given angle a_fRotAngle
	void Vec2::Rotate(const float& a_fRotAngle)
	{
		float l_fNewX = X * cosf(a_fRotAngle) + Y * sinf(a_fRotAngle);
		float l_fNewY = X * -sinf(a_fRotAngle) + Y * cosf(a_fRotAngle);
		X = l_fNewX;
		Y = l_fNewY;
	}
}