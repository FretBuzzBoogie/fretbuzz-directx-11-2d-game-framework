#pragma once
#include "..\FSM\FSM.h"
#include <type_traits>
#include "..\Timer\Timer.h"

namespace FretBuzzManager
{
	//Each scene state should be inherited from IScene
	class IScene : public FretBuzzManager::IFSM
	{
	public:
		IScene() = delete;

	protected:
		IScene(std::string a_strSceneName): 
			FretBuzzManager::IFSM(a_strSceneName)
		{
		}
	};

	//This class is not used and can only be used internally
	//The SceneData is inherited from it for the scene management
	class ISceneData : public FretBuzzManager::IFSM
	{
	public:
		ISceneData() = delete;

	protected:
		ISceneData(std::string a_strSceneID) : IFSM(a_strSceneID)
		{
		
		}

		virtual ~ISceneData()
		{}
	};

	//To create a scene state, this a scene state wtih this class should be created with SCENE_TYPE as the Scene State type.
	template<typename SCENE_TYPE, typename = typename std::enable_if<std::is_base_of<IScene, SCENE_TYPE>::value>::type >
	class SceneData : public ISceneData
	{
		private:
			SCENE_TYPE* m_pScene = nullptr;

		public:
			SceneData() = delete;

			SceneData(std::string a_strSceneID)
				: FretBuzzManager::ISceneData(a_strSceneID)
			{
				
			}

			virtual ~SceneData()
			{
				
			}

			//On entering the state it will instantiate the state of type SCENE_TYPE
			virtual void OnStateEnter() override
			{
				FretBuzzManager::IFSM::OnStateEnter();

				m_pScene = new SCENE_TYPE("SCENE_STATE::"+GetSceneName());

				FretBuzzManager::IFSM* l_pIFSM = nullptr;
				l_pIFSM = static_cast<FretBuzzManager::IFSM*>(m_pScene);

				if (l_pIFSM != nullptr)
				{
					l_pIFSM->OnStateEnter();
				}
				else
				{
					FretBuzzSystem::Debug::LogError("SceneManager::OnStateEnter::" + std::string(typeid(SCENE_TYPE).name()) + " cannot be casted to base class ' FretBuzzManager::IFSM* '");
				}
			}

			virtual void OnStateExit() override
			{
				FretBuzzManager::IFSM::OnStateExit();

				FretBuzzManager::IFSM* l_pIFSM = nullptr;
				l_pIFSM = static_cast<FretBuzzManager::IFSM*>(m_pScene);

				if (l_pIFSM != nullptr)
				{
					l_pIFSM->OnStateExit();
				}
				else
				{
					FretBuzzSystem::Debug::LogError("SceneManager::OnStateExit::" + std::string(typeid(SCENE_TYPE).name()) + " cannot be casted to base class ' FretBuzzManager::IFSM* '");
				}

				delete m_pScene;
				m_pScene = nullptr;
			}
	};

	//Specialized class for Scene Managment from FSM, could be template specialize but decided to write it separately
	//The main class for scene management
	class SceneManager : public FretBuzzManager::FSM<ISceneData>
	{
	public:
		SceneManager(ISceneData* a_pStartScene, bool a_bIsTransitionToSelfAllowed = false);
		SceneManager(std::vector<ISceneData*>& a_pVectIScene, bool a_bIsTransitionToSelfAllowed = false);

		virtual ~SceneManager();

		void RegisterState(ISceneData* a_pScene);
		void TransitionTo(std::string a_strSceneName);

	};
}