#pragma once
#include "../Math/Rect.h"
#include "../Math/Vec2.h"
#include "../Image/Sprite.h"
#include "../Math/Rect.h"
#include <string>

namespace FretBuzzEngine
{
	class Tile
	{
	private:
		FretBuzzMath::Rect m_Rect;
		FretBuzzEngine::Sprite* m_pSprite = nullptr;

	public:
		Tile(FretBuzzMath::Vec2 a_v2Pos, int a_iWidth, int a_iHeight, Sprite* a_Sprite);
		~Tile();

		void Draw(FretBuzzMath::Vec2 a_v2TileMapStart, FretBuzzMath::Rect a_Rect, FretBuzzSystem::Graphics& a_GFX);
	};
}