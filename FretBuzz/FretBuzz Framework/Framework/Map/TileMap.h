#pragma once
#include "../Camera/Renderable.h"
#include "Tile.h"
#include <unordered_map>

namespace FretBuzzEngine
{
	class TileMap : public IRenderable
	{
	public:
		typedef std::unordered_map<int, Sprite*> UMAP_SYMBL_TO_SPRITE;

	protected:

		int m_iMapTilesPerRow = 0;
		int m_iMapTilesPerCol = 0;
		int m_iTotalTiles = 0;

		int m_iSingleTileWidth = 0;
		int m_iSingleTileHeight = 0;

		static constexpr int EMPTY_TILE_SYMBOL = 0;

		FretBuzzMath::Vec2 m_v2TopLeft;

		Tile** m_ppTile = nullptr;

		void CreateTiles(const UMAP_SYMBL_TO_SPRITE& a_UMapSymbolToSprite, std::vector<int> a_vectSpriteSymbols);

	public:
		TileMap(FretBuzzMath::Vec2 a_v2MapStart, int a_iMapTilesWidth, int a_iMapTilesHeight,
			int a_iSingleTileWidth, int a_iSingleTileHeight, const UMAP_SYMBL_TO_SPRITE& m_pUMapSymbolToSprite,
			std::vector<int> a_vectSpriteSymbols);

		TileMap(FretBuzzMath::Vec2 a_v2MapStart,
			int a_iSingleTileWidth, int a_iSingleTileHeight, const UMAP_SYMBL_TO_SPRITE& m_pUMapSymbolToSprite,
			std::string a_strTileMapFileName);

		virtual ~TileMap();

		int GetTilesPerRow() const;
		int GetTilesPerCol() const;

		//Virtual function to draw the rendereable in the given way as written in the derived class
		virtual void Rasterize(FretBuzzSystem::Graphics& a_GFX);

	};
}