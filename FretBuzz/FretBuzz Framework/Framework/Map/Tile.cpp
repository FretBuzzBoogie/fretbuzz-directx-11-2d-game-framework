#pragma once
#include "../Map/TileMap.h"

namespace FretBuzzEngine
{
	Tile::Tile(FretBuzzMath::Vec2 a_v2Pos, int a_iWidth, int a_iHeight, Sprite* a_pSprite)
		: m_Rect(a_v2Pos.Y, a_v2Pos.Y + a_iHeight, a_v2Pos.X, a_v2Pos.X + a_iWidth),
		m_pSprite(a_pSprite)
	{

	}

	Tile::~Tile()
	{
	
	}

	void Tile::Draw(FretBuzzMath::Vec2 a_v2TileMapStart , FretBuzzMath::Rect a_ClipRect,FretBuzzSystem::Graphics& a_GFX)
	{
		m_pSprite->DrawSprite(a_v2TileMapStart.X + m_Rect.m_fLeft, a_v2TileMapStart.Y + m_Rect.m_fTop, a_GFX, &a_ClipRect);
	}

}