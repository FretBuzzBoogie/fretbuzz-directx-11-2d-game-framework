#pragma once
#include "TileMap.h"
#include <fstream>
#include "..\Debugging\Debug.h"
#include <sstream>
#include <string>
#include <iostream>

namespace FretBuzzEngine
{
	void TileMap::CreateTiles(const UMAP_SYMBL_TO_SPRITE& a_UMapSymbolToSprite, std::vector<int> a_vectSpriteSymbols)
	{
		m_ppTile = new Tile*[m_iTotalTiles];

		for (int l_iTileIndex = 0; l_iTileIndex < m_iTotalTiles; l_iTileIndex++)
		{
			int l_iCurrentSymbol = a_vectSpriteSymbols[l_iTileIndex];
			
			if (l_iCurrentSymbol == EMPTY_TILE_SYMBOL)
			{
				m_ppTile[l_iTileIndex] = nullptr;
				continue;
			}

			FretBuzzMath::Vec2 l_v2CurrentTilePosFromStart =
			{
				(float)(l_iTileIndex % m_iMapTilesPerRow) * m_iSingleTileWidth,
				(float)(l_iTileIndex / m_iMapTilesPerRow) * m_iSingleTileHeight
			};

			if (a_UMapSymbolToSprite.find(l_iCurrentSymbol) == a_UMapSymbolToSprite.end())
			{
				FretBuzzSystem::Debug::LogError("TileMap::CreateTiles::Could not find symbol " + std::to_string(l_iCurrentSymbol));
			}

			m_ppTile[l_iTileIndex] = new Tile(l_v2CurrentTilePosFromStart, m_iSingleTileWidth, m_iSingleTileHeight, a_UMapSymbolToSprite.at(l_iCurrentSymbol));
		}
	}

	TileMap::TileMap(FretBuzzMath::Vec2 a_v2MapStart, int a_iMapTilesPerRow, int a_iMapTilesPerCol,
		int a_iSingleTileWidth, int a_iSingleTileHeight, const UMAP_SYMBL_TO_SPRITE& a_UMapSymbolToSprite, std::vector<int> a_vectSpriteSymbols)
		:
		m_v2TopLeft(a_v2MapStart),
		m_iMapTilesPerRow(a_iMapTilesPerRow),
		m_iMapTilesPerCol(a_iMapTilesPerCol),
		m_iTotalTiles(m_iMapTilesPerRow * m_iMapTilesPerCol),

		m_iSingleTileWidth(a_iSingleTileWidth),
		m_iSingleTileHeight(a_iSingleTileHeight)
	{
		CreateTiles(a_UMapSymbolToSprite, a_vectSpriteSymbols);
	}

	TileMap::TileMap(FretBuzzMath::Vec2 a_v2MapStart,int a_iSingleTileWidth, int a_iSingleTileHeight, const UMAP_SYMBL_TO_SPRITE& a_pUMapSymbolToSprite,
		std::string a_strTileMapFileName)
		: m_v2TopLeft(a_v2MapStart),

		m_iSingleTileWidth(a_iSingleTileWidth),
		m_iSingleTileHeight(a_iSingleTileHeight)
	{
		std::ifstream l_InputStream(a_strTileMapFileName);

		if (!l_InputStream)
		{
			FretBuzzSystem::Debug::Log("TileMap::TileMap::Could not open file ::"+ a_strTileMapFileName);
			return;
		}

		std::vector<int> l_vectSpriteSymbols;

		std::string l_strParsedLine;
		
		while (getline(l_InputStream, l_strParsedLine))
		{
			std::string l_CurrentSymbol;
			std::stringstream l_CurrentStream{ l_strParsedLine };

			while(getline(l_CurrentStream, l_CurrentSymbol,','))
			{
				l_vectSpriteSymbols.push_back(std::stoi(l_CurrentSymbol));

				if (m_iMapTilesPerCol == 0)
				{
					m_iMapTilesPerRow++;
				}
			}
			m_iMapTilesPerCol++;
		}

		l_InputStream.close();

		m_iTotalTiles = m_iMapTilesPerRow * m_iMapTilesPerCol;
		CreateTiles(a_pUMapSymbolToSprite, l_vectSpriteSymbols);
	}

	TileMap::~TileMap()
	{
		if (m_ppTile != nullptr)
		{
			for (int l_iTileIndex = 0; l_iTileIndex < m_iTotalTiles; l_iTileIndex++)
			{
				delete m_ppTile[l_iTileIndex];
				m_ppTile[l_iTileIndex] = nullptr;
			}
			delete[] m_ppTile;
			m_ppTile = nullptr;
		}
	}

	int TileMap::GetTilesPerRow() const
	{
		return m_iMapTilesPerRow;
	}

	int TileMap::GetTilesPerCol() const
	{
		return m_iMapTilesPerCol;
	}

	void TileMap::Rasterize(FretBuzzSystem::Graphics& a_GFX)
	{
		FretBuzzMath::Vec2 l_v2CurrVec2Start = m_mat3Transform * m_v2TopLeft;
		for (int l_iTileIndex = 0; l_iTileIndex < m_iTotalTiles; l_iTileIndex++)
		{
			if (m_ppTile[l_iTileIndex] == nullptr)
			{
				continue;
			}
			m_ppTile[l_iTileIndex]->Draw(l_v2CurrVec2Start, m_ClipRect, a_GFX);
		}
	}
}