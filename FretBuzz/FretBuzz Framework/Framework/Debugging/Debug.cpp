#pragma once
#include "Debug.h"

#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

namespace FretBuzzSystem
{
	bool Debug::s_bIsOpenDebugConsole = false;
	
	//The singleton instance
	Debug* Debug::s_pDebug = nullptr;

	//
	Debug::Debug()
	{

	}

	//
	Debug::~Debug()
	{
	
	}

	//Destroys the singleton instance
	void Debug::Destroy()
	{
		if (s_pDebug != nullptr)
		{
			delete s_pDebug;
			s_pDebug = nullptr;
		}
	}

	//Creates the console window
	// Can be called only once
	bool Debug:: CreateConsoleWindow()
	{
		s_bIsOpenDebugConsole = true;

		FILE *l_pFile = nullptr;

		AllocConsole();
		SetConsoleTitleA("FRET_BUZZ_DEBUG_CONSOLE");

		freopen_s(&l_pFile, "CONOUT$", "w", stdout);

		/*int hConHandle;
		long lStdHandle;
		CONSOLE_SCREEN_BUFFER_INFO coninfo;
		freopen("CON", "w", stdout);
		freopen("CON", "r", stdin);
		freopen("CON", "w", stderr);

		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
		coninfo.dwSize.Y = MAX_CONSOLE_LINES;
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

		lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "r");
		if (fp == nullptr)
		{
			throw std::exception("Debug::CreateConsoleWindow:: Failed to associates an input handle stream with a file\n");
			return false;
		}
		*stdin = *fp;
		setvbuf(stdin, NULL, _IONBF, 0);


		lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		if (fp == nullptr)
		{
			throw std::exception("Debug::CreateConsoleWindow:: Failed to associates a OutPut handle stream with a file\n");
			return false;
		}
		*stdout = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);


		lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		if (fp == nullptr)
		{
			throw std::exception("Debug::CreateConsoleWindow:: Failed to associates a OutPut Error handle stream with a file\n");
			return false;
		}

		*stderr = *fp;
		setvbuf(stderr, NULL, _IONBF, 0);
		
		std::ios::sync_with_stdio();*/

		return true;
	}

	//Provides the functionality to log with the error type into the window
	void Debug::LogInternal(const std::string& a_Log, LogType a_LogType)
	{
		if (!s_bIsOpenDebugConsole)
		{
			return;
		}

		switch (a_LogType)
		{
		case LogType::LOG:
			std::cout << "LOG    ::"<<a_Log << std::endl;
			break;

		case LogType::LOG_ERROR:
			std::cout << "ERROR  ::" << a_Log << std::endl;
			break;

		case LogType::LOG_WARNING:
			std::cout << "WARNING::" << a_Log << std::endl;
			break;
		}
	}

	//Returns the Debug singleton
	//Cretaes a new object if not already crated
	Debug* const Debug::GetInstance()
	{
		if (s_pDebug == nullptr)
		{
			s_pDebug = new Debug();
		}
		return s_pDebug;
	}

	//Logs the input string as simple output
	void Debug::Log(const std::string& a_Log)
	{
		#ifdef IS_FRET_BUZZ_DEBUG
			s_pDebug->LogInternal(a_Log, LogType::LOG);
		#endif

	}

	//Logs the input string as an error
	void Debug::LogError(const std::string& a_Log)
	{
		#ifdef IS_FRET_BUZZ_DEBUG
			s_pDebug->LogInternal(a_Log, LogType::LOG_ERROR);
		#endif
	}

	//Logs the input string as a warning
	void Debug::LogWarning(const std::string& a_Log)
	{
		#ifdef IS_FRET_BUZZ_DEBUG
			s_pDebug->LogInternal(a_Log, LogType::LOG_WARNING);
		#endif
	}
}