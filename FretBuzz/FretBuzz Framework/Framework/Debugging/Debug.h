#pragma once
#include <string>
#include <windows.h>
#include <exception>

namespace FretBuzzSystem
{
	class Debug
	{
	private:
		friend class System;

		static Debug* s_pDebug;

		Debug();
		~Debug();


		enum class LogType
		{
			LOG,
			LOG_WARNING,
			LOG_ERROR
		};

		static const WORD MAX_CONSOLE_LINES = 500;
		void LogInternal(const std::string& a_Log, LogType a_LogType);

		static bool s_bIsOpenDebugConsole;
		static void Destroy();
		bool CreateConsoleWindow();

	public:
		static Debug* const GetInstance();
		
		static void Log(const std::string& a_Log);
		static void LogError(const std::string& a_Log);
		static void LogWarning(const std::string& a_Log);
	};
}