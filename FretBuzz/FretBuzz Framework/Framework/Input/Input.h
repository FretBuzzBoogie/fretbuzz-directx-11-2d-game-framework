#pragma once
#include <bitset>
#include "../Input/Input.h"
#include "../Math/Vec2.h"

namespace FretBuzzSystem
{
	class Input
	{
	private:
		static Input* s_pInstance;
		friend class System;

		Input();
		~Input();

		int m_iMouseX = 0;
		int m_iMouseY = 0;

		bool m_bIsMouseInWindow = false;
		
		static constexpr int KEY_BUFFER_SIZE = 256;
		std::bitset<KEY_BUFFER_SIZE> m_bitsetKeyBuffer;

		void OnKeyDown(const unsigned char a_KeyCode);
		void OnKeyUp(const unsigned char a_KeyCode);

		void OnMouseEnterWindow();
		void OnMouseExitWindow();
		void OnMouseMove(const int a_iXPos, const int a_iYPos);
		void OnMouseWheelScrollForward(const int a_iDelta);
		void OnMouseWheelScrollBackward(const int a_iDelta);

	public:
		static Input* const GetInstance();
		static void Destroy();

		bool IsKeyPutDown(const unsigned char a_KeyCode);
		bool IsKeyDown(const unsigned char a_KeyCode);

		bool IsKeyPutUp(const unsigned char a_KeyCode);
		bool IsKeyUp(const unsigned char a_KeyCode);

		int GetMouseX() const;
		int GetMouseY() const;

		FretBuzzMath::Vec2 GetMousePosition();
	};
}