#pragma once
#include "Input.h"

namespace FretBuzzSystem
{
	//Singletin instance
	Input* Input:: s_pInstance = nullptr; 

	//Constructor, called only once for the singleton instance
	Input::Input()
	{
		for (int l_iKeyIndex = 0; l_iKeyIndex < KEY_BUFFER_SIZE; l_iKeyIndex++)
		{
			m_bitsetKeyBuffer[l_iKeyIndex] = false;
		}
	}

	Input::~Input()
	{
	
	}

	//Called on key with char a_KeyCode down
	void Input::OnKeyDown(const unsigned char a_KeyCode)
	{
		m_bitsetKeyBuffer[a_KeyCode] = true;
	}

	//Called on key with char a_KeyCode up
	void Input::OnKeyUp(const unsigned char a_KeyCode)
	{
		m_bitsetKeyBuffer[a_KeyCode] = false;
	}

	//Event on mouse entering the window
	void Input::OnMouseEnterWindow()
	{
		
	}

	//Event on mouse exiting the window
	void Input::OnMouseExitWindow()
	{
		
	}

	void Input::OnMouseMove(const int a_iXPos, const int a_iYPos)
	{
		m_iMouseX = a_iXPos;
		m_iMouseY = a_iYPos;
	}

	void Input::OnMouseWheelScrollForward(const int a_iDelta)
	{
		
	}

	void Input::OnMouseWheelScrollBackward(const int a_iDelta)
	{

	}

	//Returns the singleton instance
	//Creates the Input if not already created
	Input* const Input::GetInstance()
	{
		if (s_pInstance == nullptr)
		{
			s_pInstance = new Input();
		}
		return s_pInstance;
	}

	//Destroys the singleton instance if exists
	void Input::Destroy()
	{
		if (s_pInstance != nullptr)
		{
			delete s_pInstance;
			s_pInstance = nullptr;
		}
	}

	bool Input::IsKeyPutUp(const unsigned char a_KeyCode)
	{
		 
		return true;
	}

	bool Input::IsKeyPutDown(const unsigned char a_KeyCode)
	{
		
		return true;
	}

	//Returns true if the key with keycode is down
	bool Input::IsKeyDown(const unsigned char a_KeyCode)
	{
		return s_pInstance->m_bitsetKeyBuffer[a_KeyCode];
	}

	//Returns true if the key with keycode is down
	bool Input::IsKeyUp(const unsigned char a_KeyCode)
	{
		return !s_pInstance->m_bitsetKeyBuffer[a_KeyCode];
	}

	//Returns X - coordinate of the mouse position
	int Input::GetMouseX() const
	{
		return m_iMouseX;
	}

	//Returns Y - coordinate of the mouse position
	int Input::GetMouseY() const
	{
		return m_iMouseY;
	}

	//Returns mouse position in the form of Vec2
	FretBuzzMath::Vec2 Input::GetMousePosition()
	{
		return { (float)m_iMouseX , (float)m_iMouseY };
	}
}