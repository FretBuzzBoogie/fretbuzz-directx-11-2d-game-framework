#pragma once
#include <iostream>
#include <Windows.h>
#include "System\System.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	using namespace FretBuzzSystem;

	System* const l_pSystem = System::GetInstance(lpCmdLine, nCmdShow);

	if(l_pSystem != nullptr)
	{
		l_pSystem->Run();
	}

	l_pSystem->Destroy();

	return 0;
}