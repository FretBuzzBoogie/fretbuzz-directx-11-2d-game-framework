#pragma once
#include "Camera.h"

namespace FretBuzzEngine
{
	Camera::Camera(const FretBuzzMath::Rect& a_Rect, FretBuzzMath::Vec2 a_v2Position, Viewport* a_pIRenderTarget)
		: IRenderTarget(a_Rect, a_pIRenderTarget, a_v2Position)
	{
	}

	Camera::~Camera()
	{
	}

	//The camera render function
	void Camera::Draw(FretBuzzEngine::IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX)
	{
		a_Renderable.m_mat3Transform = FretBuzzMath::Mat3::GetIdentity();
		a_Renderable.Transform(FretBuzzMath::Mat3::Translate(-m_V2Position));
		a_Renderable.m_ClipRect = m_Rect;

		IRenderTarget::Draw(a_Renderable, a_pGFX);
	}
}