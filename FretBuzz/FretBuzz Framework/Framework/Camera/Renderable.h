#pragma once
#include "../Math/Vec2.h"
#include "../Math/Rect.h"
#include "../Math/Mat3.h"
#include "../Graphics/Graphics.h"

namespace FretBuzzEngine
{
	class IRenderable
	{
	protected:


	public:
		FretBuzzMath::Rect m_ClipRect;
		FretBuzzMath::Mat3 m_mat3Transform;

		IRenderable()
			: m_ClipRect(0.0f, 0.0f, 0.0f, 0.0f)
		{
			
		};

		virtual ~IRenderable() = 0
		{
		}

		void Transform(const FretBuzzMath::Mat3& a_Mat3)
		{
			m_mat3Transform = a_Mat3 * m_mat3Transform;
		}

		//Clips the renderable rect with the rendertarget rect
		void Clip(const FretBuzzMath::Rect& a_WindowRect)
		{
			m_ClipRect.ClipWithRect(a_WindowRect);
		}

		//Virtual function to draw the rendereable in the given way as written in the derived class
		virtual void Rasterize(FretBuzzSystem::Graphics& a_GFX ) = 0;

	};
}