#pragma once
#include "../Math/Rect.h"
#include "RenderTarget.h"
#include "Viewport.h"
#include "../Graphics/Graphics.h"
#include "../Camera/Renderable.h"

namespace FretBuzzEngine
{
	class Camera : public IRenderTarget 
	{
	private:

	public:
		Camera(const FretBuzzMath::Rect& a_Rect, FretBuzzMath::Vec2 a_v2Position, Viewport* a_pIRenderTarget = nullptr);
		~Camera();
		virtual void Draw(FretBuzzEngine::IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX) override;

		/*void Draw(FretBuzzEngine::IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX)
		{
			Draw(a_Renderable, a_pGFX,m_Rect);
		}*/
	};
}