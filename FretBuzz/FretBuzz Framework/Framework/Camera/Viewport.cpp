#pragma once
#include "Viewport.h"

namespace FretBuzzEngine
{
	Viewport::Viewport(const FretBuzzMath::Rect& a_Rect, Viewport* a_pViewport)
		: IRenderTarget(a_Rect, a_pViewport, {a_Rect.m_fLeft, a_Rect.m_fTop})
	{
		
	}

	Viewport::~Viewport()
	{
	
	}

	//The camera render function
	void Viewport::Draw(IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX)
	{
		a_Renderable.m_ClipRect.ClipWithRect(m_Rect);
		a_Renderable.Transform(FretBuzzMath::Mat3::Translate(m_V2Position));
		IRenderTarget::Draw(a_Renderable, a_pGFX);
	}
}