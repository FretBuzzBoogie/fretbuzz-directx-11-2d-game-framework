#pragma once
#include "../Math/Rect.h"
#include "RenderTarget.h"
#include "../Graphics/Graphics.h"

namespace FretBuzzEngine
{
	class Viewport : public IRenderTarget
	{
	public:
		Viewport(const FretBuzzMath::Rect& a_Rect, Viewport* a_Viewport = nullptr);
		~Viewport();

		//The camera render function
		virtual void Draw(IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX) override;

	};
}