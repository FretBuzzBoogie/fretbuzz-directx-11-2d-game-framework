#pragma once
#include "../Math/Rect.h"
#include "Renderable.h"
#include "../Graphics/Graphics.h"

namespace FretBuzzEngine
{
	class IRenderTarget
	{
	protected:
		FretBuzzMath::Rect m_Rect;
		IRenderTarget* m_pNextRenderTarget = nullptr;

	public:
		FretBuzzMath::Vec2 m_V2Position{ 0.0f, 0.0f };

		IRenderTarget(const FretBuzzMath::Rect& a_Rect, IRenderTarget* const m_pNextRenderTarget, FretBuzzMath::Vec2 a_V2Position )
			:m_Rect {a_Rect},
			m_pNextRenderTarget{ m_pNextRenderTarget },
			m_V2Position{a_V2Position }
		{

		}

		virtual ~IRenderTarget() = 0
		{
		}

		//Moves the camera rect by given position
		void MoveTarget(const FretBuzzMath::Vec2& a_V2NewPosition)
		{
			m_V2Position = {a_V2NewPosition.X - m_Rect.GetWidth() / 2.0f,
				a_V2NewPosition.Y - m_Rect.GetHeight() / 2.0f};
		}

		virtual void Draw(IRenderable& a_Renderable, FretBuzzSystem::Graphics* a_pGFX) = 0
		{
			if (m_pNextRenderTarget != nullptr)
			{
				m_pNextRenderTarget->Draw(a_Renderable, a_pGFX);
			}
			else
			{
				a_Renderable.Rasterize(*a_pGFX);
			}
		}
	};
}