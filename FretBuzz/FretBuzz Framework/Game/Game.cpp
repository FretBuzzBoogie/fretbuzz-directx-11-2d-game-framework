#pragma once
#include "Game.h"
#include <array>
#include "../Framework/Math/Vertex2D.h"

namespace FretBuzzSystem
{
	Game::Game(FretBuzzSystem::Graphics* const a_pGFX)
		: m_pInput{ FretBuzzSystem::Input::GetInstance() },
		m_pGFX{ a_pGFX },
		IUpdateTimer(),
		m_vectScenes{ }
	{

		m_vectScenes.emplace_back(m_pInitSceneData);
		m_vectScenes.emplace_back(m_pMainMenuScene);
		m_vectScenes.emplace_back(m_pScoreState);

		m_pSceneManager = new FretBuzzManager::SceneManager(m_vectScenes);

		//////////////////////////////////////////////////
		m_pPlayer = new TestPlayer({ 400,300 }, "Hotline\\WalkUnarmed_strip8.png");
		//m_pPlayer2 = new TestPlayer({ 0,0}, "barnacle.png", false);

		m_pViewport = new FretBuzzEngine::Viewport(FretBuzzMath::Rect(0.0f, System::GetScreenHeight() - 1, 0.0f, System::GetScreenWidth() - 1));
		m_pCamera = new FretBuzzEngine::Camera(FretBuzzMath::Rect(0.0f, System::GetScreenHeight() - 1, 0.0f, System::GetScreenWidth() - 1), { 0.0, 0.0 }, m_pViewport);

		m_pSprite1 = new FretBuzzEngine::Sprite(std::string("Back1.png"));
		m_pSprite2 = new FretBuzzEngine::Sprite(std::string("Back2.png"));

		std::unordered_map<int, FretBuzzEngine::Sprite*> m_pUMap =
		{
			{ 11, m_pSprite1 },
			{ 22, m_pSprite2 }
		};

		m_pTileMap = new FretBuzzEngine::TileMap({ 0.0f, 0.0f }, 146, 147, m_pUMap, "Resources//TileMaps//Tmap.txt");

		std::array<FretBuzzMath::Vec2, 4> l_arrPosition = { FretBuzzMath::Vec2(-200.0f, -200.0f),FretBuzzMath::Vec2{ 200.0f, -200.0f },FretBuzzMath::Vec2{ 200.0f, 200.0f },FretBuzzMath::Vec2{ -200.0f, 200.0f } };

		m_pTex2D = new FretBuzzEngine::Tex2D("Attack__000.png");
		//m_pSpriteSheet = new FretBuzzEngine::Spritesheet("Hotline\\HumanShieldShoot.png",2, 2, 1, true, PixelColor(0x00008040));
	}

	Game::~Game()
	{
		if (m_pSpriteAnim != nullptr)
		{
			delete m_pSpriteAnim;
			m_pSpriteAnim = nullptr;
		}

		if (m_pSprite != nullptr)
		{
			delete m_pSprite;
			m_pSprite = nullptr;
		}

		if (m_pPoly != nullptr)
		{
			delete m_pPoly;
			m_pPoly = nullptr;
		}

		if (m_pCamera != nullptr)
		{
			delete m_pCamera;
			m_pCamera = nullptr;
		}

		if (m_pViewport != nullptr)
		{
			delete m_pViewport;
			m_pViewport = nullptr;
		}
		
		if (m_pPlayer != nullptr)
		{
			delete m_pPlayer;
			m_pPlayer = nullptr;
		}

		if (m_pTileMap != nullptr)
		{
			delete m_pTileMap;
			m_pTileMap = nullptr;
		}

		if (m_pSpriteSheet != nullptr)
		{
			delete m_pSpriteSheet;
			m_pSpriteSheet = nullptr;
		}

		if (m_pTex2D != nullptr)
		{
			delete m_pTex2D;
			m_pTex2D = nullptr;
		}

		if (m_pSceneManager != nullptr)
		{
			delete m_pSceneManager;
			m_pSceneManager = nullptr;
		}

		if (m_pInitSceneData != nullptr)
		{
			delete m_pInitSceneData;
			m_pInitSceneData = nullptr;
		}

		if (m_pMainMenuScene != nullptr)
		{
			delete m_pMainMenuScene;
			m_pMainMenuScene = nullptr;
		}

		if (m_pScoreState != nullptr)
		{
			delete m_pScoreState;
			m_pScoreState = nullptr;
		}
	}

	void Game::OnUpdate(const float& a_fDeltaTime) 
	{
		//m_pSpriteAnim->OnUpdateAnimation(a_fDeltaTime);

		if(m_pInput->IsKeyDown('S'))
		{
			m_pPlayer->m_Position.Y += m_pPlayer->m_fSpeed * a_fDeltaTime;
		}
		if (m_pInput->IsKeyDown('W'))
		{
			m_pPlayer->m_Position.Y -= m_pPlayer->m_fSpeed * a_fDeltaTime;
		}
		if (m_pInput->IsKeyDown('A'))
		{
			m_pPlayer->m_Position.X -= m_pPlayer->m_fSpeed * a_fDeltaTime;
		}
		if (m_pInput->IsKeyDown('D'))
		{
			m_pPlayer->m_Position.X += m_pPlayer->m_fSpeed * a_fDeltaTime;
		}
		
		if (m_pInput->IsKeyDown('T'))
		{
			m_fTexScale += 0.1f;
		}
		else if (m_pInput->IsKeyDown('Y'))
		{
			m_fTexScale -= 0.1f;
		}

		if (m_pInput->IsKeyDown('M'))
		{
			m_pSceneManager->TransitionTo("mainmenu");
		}

		if (m_pInput->IsKeyDown('N'))
		{
			m_pSceneManager->TransitionTo("scorestate");
		}

		if (m_pInput->IsKeyDown('I'))
		{
			m_pSceneManager->TransitionTo("init");
		}
		//m_pCamera->MoveTarget({ m_pPlayer->m_Position.X, m_pPlayer->m_Position.Y });
	}

	void Game::OnLateUpdate(const float& a_fDeltaTime)
	{
		m_pCamera->MoveTarget(m_pPlayer->m_Position);
		m_pCamera->Draw(*m_pTileMap, m_pGFX);
		m_pCamera->Draw(*m_pPlayer, m_pGFX);

		m_fTexAngle += (a_fDeltaTime *  M_PI / 5.0f);

		FretBuzzMath::Mat3 l_m3Pos = FretBuzzMath::Mat3::Translate({ (float)m_pInput->GetMouseX(),(float)m_pInput->GetMouseY() }) * FretBuzzMath::Mat3::Rotate(m_fTexAngle) * FretBuzzMath::Mat3::Scale(m_fTexScale);

		m_pTex2D->DrawSprite(l_m3Pos, *m_pGFX, new FretBuzzMath::Rect(0.0f, FretBuzzSystem::System::GetScreenHeight() - 1, 0.0f, FretBuzzSystem::System::GetScreenWidth() - 1));

	}
}