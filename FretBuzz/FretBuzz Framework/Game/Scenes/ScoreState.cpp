#pragma once
#include "ScoreState.h"

namespace FretBuzzScenes
{
	ScoreState::ScoreState(std::string a_strSceneName)
		:FretBuzzManager::IScene(a_strSceneName),
		IUpdateTimer()
	{
	}

	ScoreState:: ~ScoreState()
	{

	}

	void ScoreState::OnStateEnter()
	{
		FretBuzzManager::IFSM::OnStateEnter();
	}

	void ScoreState::OnStateExit()
	{
		FretBuzzManager::IFSM::OnStateExit();
	}

	void ScoreState::OnUpdate(const float& a_fDeltaTime)
	{

	}

	void ScoreState::OnLateUpdate(const float& a_fDeltaTime)
	{

	}
}