#pragma once
#include "..\..\Framework\Managers\SceneManager.h"

namespace FretBuzzScenes
{
	class Gameplay : public FretBuzzManager::IScene, public FretBuzzSystem::IUpdateTimer
	{
	public:
		Gameplay(std::string a_strSceneName);

		virtual ~Gameplay();

		virtual void OnStateEnter() override;
		virtual void OnStateExit() override;
		void Gameplay::OnUpdate(const float& a_fDeltaTime);
		void Gameplay::OnLateUpdate(const float& a_fDeltaTime);
	};
}