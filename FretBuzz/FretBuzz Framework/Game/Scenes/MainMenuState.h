#pragma once
#include "..\..\Framework\Managers\SceneManager.h"

namespace FretBuzzScenes
{
	class MainMenuState : public FretBuzzManager::IScene, public FretBuzzSystem::IUpdateTimer
	{
	public:
		MainMenuState(std::string a_strSceneName);
		virtual ~MainMenuState();

		virtual void OnStateEnter() override;
		virtual void OnStateExit() override;

		void OnUpdate(const float& a_fDeltaTime);
		void OnLateUpdate(const float& a_fDeltaTime);
	};
}