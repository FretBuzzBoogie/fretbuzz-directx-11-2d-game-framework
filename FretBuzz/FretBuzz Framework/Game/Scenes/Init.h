#pragma once
#include "..\..\Framework\Managers\SceneManager.h"

namespace FretBuzzScenes
{
	class Init : public FretBuzzManager::IScene, public FretBuzzSystem::IUpdateTimer
	{
	public:
		Init(std::string a_strSceneName);
		virtual ~Init();

		virtual void OnStateEnter() override;
		virtual void OnStateExit() override;

		void OnUpdate(const float& a_fDeltaTime);
		void OnLateUpdate(const float& a_fDeltaTime);
	};
}