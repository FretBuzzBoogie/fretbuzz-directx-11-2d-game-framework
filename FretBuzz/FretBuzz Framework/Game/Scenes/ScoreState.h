#pragma once
#include "..\..\Framework\Managers\SceneManager.h"

namespace FretBuzzScenes
{
	class ScoreState : public FretBuzzManager::IScene, public FretBuzzSystem::IUpdateTimer
	{
	public:
		ScoreState(std::string a_strSceneName);

		virtual ~ScoreState();

		virtual void OnStateEnter() override;

		virtual void OnStateExit() override;

		void OnUpdate(const float& a_fDeltaTime);

		void OnLateUpdate(const float& a_fDeltaTime);
	};
}