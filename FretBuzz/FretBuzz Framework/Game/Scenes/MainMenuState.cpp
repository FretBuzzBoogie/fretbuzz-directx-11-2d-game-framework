#pragma once
#include "MainMenuState.h"

namespace FretBuzzScenes
{
	MainMenuState::MainMenuState(std::string a_strSceneName)
		:FretBuzzManager::IScene(a_strSceneName),
		IUpdateTimer()
	{
	}

	MainMenuState:: ~MainMenuState()
	{

	}

	void MainMenuState::OnStateEnter()
	{
		FretBuzzManager::IFSM::OnStateEnter();
	}

	void MainMenuState::OnStateExit()
	{
		FretBuzzManager::IFSM::OnStateExit();
	}

	void MainMenuState::OnUpdate(const float& a_fDeltaTime)
	{

	}

	void MainMenuState::OnLateUpdate(const float& a_fDeltaTime)
	{

	}
}