#pragma once
#include "Init.h"

namespace FretBuzzScenes
{
	Init::Init(std::string a_strSceneName)
		:FretBuzzManager::IScene(a_strSceneName),
		IUpdateTimer()
	{
	}

	Init:: ~Init()
	{

	}

	void Init::OnStateEnter()
	{
		FretBuzzManager::IFSM::OnStateEnter();
	}

	void Init::OnStateExit()
	{
		FretBuzzManager::IFSM::OnStateExit();
	}

	void Init::OnUpdate(const float& a_fDeltaTime)
	{

	}

	void Init::OnLateUpdate(const float& a_fDeltaTime)
	{

	}
}