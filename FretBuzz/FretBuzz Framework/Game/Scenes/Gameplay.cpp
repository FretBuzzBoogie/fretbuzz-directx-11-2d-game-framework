#pragma once
#include "Gameplay.h"

namespace FretBuzzScenes
{
	Gameplay::Gameplay(std::string a_strSceneName)
		:FretBuzzManager::IScene(a_strSceneName),
		IUpdateTimer()
	{
	}

	Gameplay:: ~Gameplay()
	{
	
	}

	void Gameplay::OnStateEnter()
	{
		FretBuzzManager::IFSM::OnStateEnter();
	}

	void Gameplay::OnStateExit()
	{
		FretBuzzManager::IFSM::OnStateExit();
	}

	void Gameplay::OnUpdate(const float& a_fDeltaTime)
	{

	}

	void Gameplay::OnLateUpdate(const float& a_fDeltaTime)
	{

	}
}