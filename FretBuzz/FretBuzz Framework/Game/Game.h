#pragma once
#include "../Framework/Graphics/Graphics.h"
#include "../Framework/Image/SpriteAnimation.h"
#include "../Framework/Timer/Timer.h"
#include "../Framework/Debugging/Debug.h"
#include "../Framework/Image/Poly.h"
#include "../Framework/Camera/Camera.h"
#include "../Framework/Camera/Renderable.h"
#include "../Framework/Math/Vec2.h"
#include "../Framework/Map/TileMap.h"
#include "../Framework/Image/Spritesheet.h"
#include "../Framework/Image/Tex2D.h"
#include "../Framework/Managers/SceneManager.h"
#include "Scenes\Init.h"
#include "Scenes\MainMenuState.h"
#include "Scenes\ScoreState.h"
#include <cmath>
#include <string>

namespace FretBuzzSystem
{
	class TestClass1 : public IUpdateTimer
	{
	public:
		TestClass1() : IUpdateTimer(true)
		{}

		// Inherited via IUpdateTimer
		void TestClass1::OnUpdate(const float & a_fDeltaTime)
		{
		}

		void TestClass1::OnLateUpdate(const float & a_fDeltaTime)
		{
		}
	};

	class TestPlayer : public FretBuzzEngine::IRenderable,
			 public IUpdateTimer
	{
	private:
		FretBuzzEngine::SpriteAnimation* m_pSpriteAnim = nullptr;
		FretBuzzEngine::Spritesheet* m_pSpriteSheet = nullptr;

	public:
		FretBuzzMath::Vec2 m_Position{0.0f, 0.0f};
		float m_fSpeed = 200.0f;

		TestPlayer(FretBuzzMath::Vec2 a_Vec2 , std::string a_Filename, bool a_bIsSpriteAnimAttack = true)
			:m_Position{ a_Vec2 }
			, IUpdateTimer(),
			IRenderable()
		{
			m_pSpriteSheet = new FretBuzzEngine::Spritesheet(a_Filename, 8, 8, 1, true, true, PixelColor(0x00008040));
			if (a_bIsSpriteAnimAttack)
			{
				m_pSpriteAnim = new FretBuzzEngine::SpriteAnimation(m_pSpriteSheet, 0.07 );
			}
			else
			{
				m_pSpriteAnim = new FretBuzzEngine::SpriteAnimation(1, a_Filename, 0.07, FretBuzzEngine::InvertType::INVERT_HORIZONTAL);
			}

		}

		~TestPlayer()
		{
			delete m_pSpriteAnim;
			m_pSpriteAnim = nullptr;

			delete m_pSpriteSheet;
			m_pSpriteSheet = nullptr;
		}

		virtual void Rasterize(FretBuzzSystem::Graphics& a_GFX) override
		{
			FretBuzzMath::Vec2 l_v2Mouse = Input::GetInstance()->GetMousePosition();

			FretBuzzMath::Vec2 m_Pos = m_mat3Transform * m_Position;

			FretBuzzMath::Vec2 l_v2PlayerToMouseVector = l_v2Mouse - m_Pos;

			float l_fAngle = acos(l_v2PlayerToMouseVector.GetNormalize().X);

			if (l_v2PlayerToMouseVector.Y > 0.0f)
			{
				l_fAngle = (2.0f * M_PI) - l_fAngle;
			}

			//m_pSpriteAnim->DrawAnimation(m_Position.X, m_Position.Y, a_GFX, &m_ClipRect);
			
			m_pSpriteAnim->DrawAnimation(FretBuzzMath::Mat3::Translate(m_Pos) *  FretBuzzMath::Mat3::Rotate(l_fAngle), a_GFX, &m_ClipRect);
		}

		// Inherited via IUpdateTimer
		void OnUpdate(const float & a_fDeltaTime)
		{
			m_pSpriteAnim->OnUpdateAnimation(a_fDeltaTime);
		}

		void OnLateUpdate(const float & a_fDeltaTime)
		{
		}
	};


	class TestMap
	{
	public:
		FretBuzzEngine::TileMap* m_TileMap = nullptr;

		TestMap()
		{
			FretBuzzEngine::TileMap::UMAP_SYMBL_TO_SPRITE l_pUMAP;


			//m_TileMap = new FretBuzzEngine::TileMap({0,0},40,40, 50,50,  );

		}

		~TestMap()
		{
			if (m_TileMap != nullptr)
			{
				delete m_TileMap;
				m_TileMap = nullptr;
			}
		}
	};


	class Game : public IUpdateTimer
	{
	private:
		FretBuzzManager::SceneManager* m_pSceneManager = nullptr;

		std::vector<FretBuzzManager::ISceneData*> m_vectScenes;
		
		FretBuzzManager::SceneData<FretBuzzScenes::Init>* m_pInitSceneData = new FretBuzzManager::SceneData<FretBuzzScenes::Init>("init");
		FretBuzzManager::SceneData<FretBuzzScenes::MainMenuState>* m_pMainMenuScene = new FretBuzzManager::SceneData<FretBuzzScenes::MainMenuState>("mainmenu");
		FretBuzzManager::SceneData<FretBuzzScenes::ScoreState>* m_pScoreState = new FretBuzzManager::SceneData<FretBuzzScenes::ScoreState>("scorestate");

		FretBuzzSystem::Graphics* const m_pGFX = nullptr;
		FretBuzzSystem::Input* const m_pInput = nullptr;

	public:
		Game(FretBuzzSystem::Graphics* const a_pGFX);
		~Game();

		virtual void OnUpdate(const float & a_fDeltaTime) override;
		virtual void OnLateUpdate(const float & a_fDeltaTime) override;

		FretBuzzEngine::SpriteAnimation* m_pSpriteAnim = nullptr;
		FretBuzzEngine::Sprite* m_pSprite = nullptr;
		FretBuzzEngine::Poly* m_pPoly = nullptr;
		FretBuzzEngine::Camera* m_pCamera = nullptr;
		FretBuzzEngine::Viewport* m_pViewport = nullptr;

		TestPlayer* m_pPlayer = nullptr;
		TestPlayer* m_pPlayer2 = nullptr;

		FretBuzzEngine::Sprite* m_pSprite1 = nullptr;
		FretBuzzEngine::Sprite* m_pSprite2 = nullptr;


		FretBuzzEngine::TileMap* m_pTileMap = nullptr;
		FretBuzzEngine::Spritesheet* m_pSpriteSheet = nullptr;

		FretBuzzEngine::Tex2D* m_pTex2D = nullptr;
		float m_fTexAngle = 0.0f;
		float m_fTexScale = 1.0f;
	};
}