#Readme#

* This is a basic framework built upon DirectX 11.
  It features the functionality required to create 2D games.

### Latest Builds ###
* [Build link](https://www.dropbox.com/sh/xlde1z2hcokrt76/AADGsRTPDoSpSubN7D6cgYmEa?dl=0)
* Controls :: Mouse movement for player face position, W,A,S,D to move around, T and Y to zoom- in/out respectively.

### What is this repository for? ###
* Similar to some of my previous repo's, this too was made for the sole purpose of understanding how someone would go about creating
  an engine or framework. I will eventually try and create games based upon this framework and you can to.

### How do I get setup? ###
* Prior to opening the solution, you will need to install the DirectX SDK titled DXSDK_Jun10, click on the link :- [DirectX_SDK](https://www.microsoft.com/en-in/download/details.aspx?id=6812)
* The framework can run in x86 or x64, you can set your compiler as required.
* After you clone the repo, for now the 'Graphics.h' file consists of a variable 'IS_FULL_SCREEN'. Set it to true or false if you need your game in 
  Full screen or Windowed respectively. If in windowed, set the variables in the 'System.h' file 's_uiScreenWidth' and 's_uiScreenHeight'
  to the dimensions you desire.
* If you want to open the console window by default, set the command line arguement 'debug' into the config properties -> Debugging -> Command Arguements.
* If you want to disable logging, remove the preprocessor directive IS_FRET_BUZZ_DEBUG from  config properties -> C/C++ -> Preprocessor -> Preprocessor Definitions.
* The Game loop from where you decide to start your game will be called from the 'Render' function withing the 'Graphics.cpp'
  source file. You can put in your code between the 'BeginFrame' and 'EndFrame'.
* The folder Resources will be copied as is into the build, it includes Sprites and other resources required by the game at run time.
* The 'Game' folder includes the interface from where you need to start the game.
* The framework includes cmd arguements 'debug' to enable the 'Debug console window'. 
  Also cmd 'fullscreen' or 'windowed' to start the build in 'fullscreen' or 'windowed' respectively.

### Resources ###
* [LodePNG](http://lodev.org/lodepng/) :: Library used to load PNG images.
  The Framework/Image/ImageLoader.h takes the PNG image and stores it into the (FretBuzzSystem::PixelColor*).
  
### Sources ###
* The following are the sources from where I researched the workings of DirectX 11 and the API in general,
  they have a lot more to offer. If you stumbled upon this repo and wish to learn more, 
  you need to check them out.

* [Rastertek](http://www.rastertek.com/tutdx11.html)
* [Beginning_DirectX_11_Game_Programming](https://www.amazon.com/Beginning-DirectX-11-Game-Programming/dp/1435458958)
* [directxtutorial](http://www.directxtutorial.com/LessonList.aspx?listid=11)

### Contact ###
* rohan31f@gmail.com